import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/comman/app_themes.dart';
import 'package:flutter_app/comman/color_extenstion.dart';
import 'package:flutter_app/model/itemListData.dart';
import 'package:full_screen_image/full_screen_image.dart';
import "package:page_view_indicators/circle_page_indicator.dart";

class ViewPagerScreen extends StatefulWidget {
  @override
  _ViewPagerScreenState createState() => _ViewPagerScreenState();
}

class _ViewPagerScreenState extends State<ViewPagerScreen> {
  double currentPage = 0;

  @override
  void initState() {
    _pageController.addListener(() {
      setState(() {
        currentPage = _pageController.page;
      });
    });
    super.initState();
  }

  /*final _items = [
    Colors.blue,
    Colors.orange,
    Colors.green,
    Colors.pink,
    Colors.red,
    Colors.amber,
    Colors.brown,
    Colors.yellow,
    Colors.blue,
  ];*/

  final _pageController = PageController();
  final _currentPageNotifier = ValueNotifier<int>(0);
  final _boxHeight = 200.0;

  Widget fullScreenImage() => FullScreenWidget(
    child: ClipRRect(
      borderRadius: BorderRadius.circular(16),
      child: Image.asset(
        "assets/images/logo.png",
        fit: BoxFit.cover,
      ),
    ),
  );


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*appBar: AppBar(
        backgroundColor: AppThemes.mainAppColor,
        title: Text(
          "View Pager",
          style: TextStyle(
            fontSize: 12,
            color: Colors.white,
          ),
        ),
        centerTitle: true,
      ),*/
      body: _buildBody(),
    );
  }

  _buildBody() {
    return Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            _buildPageView(),
            _buildCircleIndicator(),
          ],
        ),
        SizedBox(
          height: 2,
        ),
        Divider(
          height: 2,
          color: Colors.green,
        ),
        FlatButton(
          onPressed: () {
            print("on clickeddddddddddddddd----");
           /* FullScreenWidget(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16),
                child: Image.asset(
                  "assets/images/bike_blc.jpg",
                  fit: BoxFit.cover,
                ),
              ),
            );*/
            fullScreenImage();
          },
          child: Text('Zoom Image'),
        )
        //Todo: Different type of indicator create.....
        /* Expanded(
          child: ListView(
            children: <Widget>[
              Text('size: 12.0, selectedSize: 16.0'),
              _buildCircleIndicator2(),
              Text('selectedDotColor: Colors.green'),
              _buildCircleIndicator3(),
              Text('dotColor: Colors.red'),
              _buildCircleIndicator4(),
              Text('selectedDotColor:Colors.blue, dotColor: Colors.black'),
              _buildCircleIndicator5(),
              Text('dotSpacing: 30.0'),
              _buildCircleIndicator6(),
              Text('wrap is fun'),
              _buildCircleIndicator7(),
            ]
                .map((item) => Padding(
                      child: item,
                      padding: EdgeInsets.all(8.0),
                    ))
                .toList(),
          ),
        ),*/
      ],
    );
  }

  final _items = ItemListData.listCategoryIcons;

  _buildPageView() {
    return Container(
      color: Colors.black12,
      height: _boxHeight,
      child: PageView.builder(
          itemCount: _items.length,
          controller: _pageController,
          itemBuilder: (BuildContext context, int index) {
            return Center(
              child: Container(
                height: double.maxFinite,
                //// USE THIS FOR THE MATCH WIDTH AND HEIGHT
                width: double.maxFinite,
                //Different type of set image size: fit: method thru
                child: IconButton(
                  iconSize: 100,
                  icon: Image.asset(_items[index], fit: BoxFit.fill),
                  onPressed: () {
                    GestureDetector(
                      onTap: () async {
                        await showDialog(
                          context: context,
                          builder: (_) => ImageDialog(),
                        );
                      },
                    );
                  },
                ),
              ),
              /*child: FlutterLogo(
                textColor: _items[index],
                size: 50.0,
              ),*/
            );
          },
          onPageChanged: (int index) {
            _currentPageNotifier.value = index;
          }),
    );
  }

  _buildCircleIndicator() {
    return Positioned(
      left: 0.0,
      right: 0.0,
      bottom: 0.0,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: CirclePageIndicator(
          size: 12.0,
          dotColor: Colors.deepOrange,
          selectedDotColor: Colors.blue,
          selectedSize: 18.0,
          itemCount: _items.length,
          currentPageNotifier: _currentPageNotifier,
        ),
      ),
    );
  }

//Todo: Different type of indicator...
/*_buildCircleIndicator2() {
    return CirclePageIndicator(
      size: 12.0,
      itemCount: _items.length,
      currentPageNotifier: _currentPageNotifier,
    );
  }

  _buildCircleIndicator3() {
    return CirclePageIndicator(
      selectedDotColor: Colors.green,
      itemCount: _items.length,
      currentPageNotifier: _currentPageNotifier,
    );
  }

  _buildCircleIndicator4() {
    return CirclePageIndicator(
      dotColor: Colors.red,
      itemCount: _items.length,
      currentPageNotifier: _currentPageNotifier,
    );
  }

  _buildCircleIndicator5() {
    return CirclePageIndicator(
      dotColor: Colors.black,
      selectedDotColor: Colors.blue,
      itemCount: _items.length,
      currentPageNotifier: _currentPageNotifier,
    );
  }

  _buildCircleIndicator6() {
    return CirclePageIndicator(
      dotSpacing: 30.0,
      itemCount: _items.length,
      currentPageNotifier: _currentPageNotifier,
    );
  }

  _buildCircleIndicator7() {
    return CirclePageIndicator(
      size: 50.0,
      selectedSize: 75.0,
      itemCount: _items.length,
      currentPageNotifier: _currentPageNotifier,
    );
  }*/
}

class ImageDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("item clickedddddddddddd DIalog---=-=>");
    return Dialog(
      child: Container(
        /*width: 200,
        height: 200,*/
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: ExactAssetImage('assets/images/bike_blc.jpg'),
                fit: BoxFit.fill)),
      ),
    );
  }
}
