import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabTextDataScreen extends StatefulWidget {
  @override
  _TabTextDataScreenState createState() => _TabTextDataScreenState();
}

class _TabTextDataScreenState extends State<TabTextDataScreen>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectIndex = 0;

  /*List<Widget> tabImageList = [
    Tab(icon: Icon(Icons.card_travel)),
    Tab(icon: Icon(Icons.add_shopping_cart)),
    Tab(icon: Icon(Icons.add)),
  ];*/

  List<Tab> tabTextList = [
    Tab(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text('Flight'),
          IconButton(
            icon: Icon(Icons.card_travel),
          ),
        ],
      ),
    ),
    Tab(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text('Train'),
          IconButton(
            icon: Icon(Icons.card_travel),
          ),
        ],
      ),
    ),
    Tab(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text('Car'),
          IconButton(
            icon: Icon(Icons.card_travel),
          ),
        ],
      ),
    ),
    Tab(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text('Cycle'),
          IconButton(
            icon: Icon(Icons.card_travel),
          ),
        ],
      ),
    ),
    Tab(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text('Boat'),
          IconButton(
            icon: Icon(Icons.card_travel),
          ),
        ],
      ),
    ),
  ];

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: tabTextList.length, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectIndex = _controller.index;
      });
      print("Select Index :: " + _controller.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      home: Scaffold(
        resizeToAvoidBottomInset : true,
        appBar: AppBar(
          bottom: TabBar(
            isScrollable: true,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 1,
            indicatorColor: Colors.red,
            onTap: (index) {
              print("clicked tab position----====-->");
            },
            controller: _controller,
            tabs: tabTextList,
          ),
        ),
        body: TabBarView(
          controller: _controller,
          children: tabTextList.map((Widget tab) {
            return new Center(
                child: Text(
              tab.toString(),
              style: TextStyle(
                fontSize: 30,
                fontStyle: FontStyle.italic,
              ),
            ));
          }).toList(),
          //Todo:Single View Create as per clicked tab related
          /*children: [
            Center(
              child: Text(
                _selectIndex.toString(),
                style: TextStyle(fontSize: 40),
              ),
            ),
            Center(
              child: Text(
                _selectIndex.toString(),
                style: TextStyle(fontSize: 40),
              ),
            ),
            Center(
              child: Text(
                _selectIndex.toString(),
                style: TextStyle(fontSize: 40),
              ),
            ),
          ],*/
        ),
      ),
    );
  }
}
