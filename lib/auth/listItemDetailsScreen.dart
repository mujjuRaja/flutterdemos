import 'package:flutter/cupertino.dart';
import 'package:flutter_app/model/itemListData.dart';

class ListItemDetailsScreen extends StatelessWidget {
  final int indexPositionData;

  ListItemDetailsScreen({Key key, this.indexPositionData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("position-=-=->>>>>>>"+indexPositionData.toString());
    return Container(
      padding: EdgeInsets.all(20),
      child: Text(
        "Press Clicked Data Show::" +
            ItemListData.listTitles[indexPositionData],
        style: TextStyle(fontSize: 25),
      ),
    );
  }
}
