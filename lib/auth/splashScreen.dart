import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_app/auth/sam/NewHomeScreen.dart';
import 'package:flutter_app/auth/loginScreen.dart';
import 'package:flutter_app/auth/sam/samloginScreen.dart';
import '../auth/userAuthScreen.dart';
import '../comman/pref_utils.dart';
import '../model/response_login.dart';
import '../comman/commonClass.dart';
import '../comman/intel_utility.dart';

class SplashScreen extends StatefulWidget {

  static const String tag = "/splash";

  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  var _visible = true;

  AnimationController animationController;
  Animation<double> animation;

  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    //  Navigator.of(context).pushReplacementNamed(UserAuthScreen());

    Preferences.getBool(Preferences.isLogin).then((pref) {
      print("prefer data-----==-> " + pref.toString());

      if (pref != null) {
        IntelUtility.navigateReplaceToScreen(context, NewHomeScreen());
        //IntelUtility.navigateAndRemoveNavigation(context, NewHomeScreen.tag);
      } else {
        IntelUtility.navigateReplaceToScreen(context, SamLoginScreen());
        //IntelUtility.navigateAndRemoveNavigation(context, LoginScreen.tag);
      }
    });

    //Navigator.push(context, MaterialPageRoute(builder: (context) => UserAuthScreen()));
  }

  @override
  dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    animationController = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: 2),
    );
    animation =
        new CurvedAnimation(parent: animationController, curve: Curves.easeOut);

    animation.addListener(() => this.setState(() {}));
    animationController.forward();

    setState(() {
      _visible = !_visible;
    });

    //redirectToScreen();
    startTime();
  }

  redirectToScreen() {
    Timer(Duration(seconds: 3), () {
      Preferences.getString(Preferences.userDetails).then((pref) {
        print("auth data----===-> " + pref);

        if (pref != null) {
          UserDetails userDetails = responseLogInUserFromJson(pref);
          CommonClass.authUser = userDetails;
          //Constant.isUserSkipLogin = false;
          CommonClass.isUserLoggedIn = true;
          //IntelUtility.navigateReplaceToScreen(context, NewHomeScreen());
          IntelUtility.navigateReplaceToScreen(context, NewHomeScreen());
        } else {
          //IntelUtility.navigateReplaceToScreen(context, LoginScreen());
          IntelUtility.navigateReplaceToScreen(context, SamLoginScreen());
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Column(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: 30.0),
                child: new Image.asset(
                  'assets/images/powered_by.png',
                  height: 25.0,
                  fit: BoxFit.scaleDown,
                ),
              )
            ],
          ),
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image.asset(
                'assets/images/logo.png',
                width: animation.value * 250,
                height: animation.value * 250,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
