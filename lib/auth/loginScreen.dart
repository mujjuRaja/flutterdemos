import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/auth/sam/samloginScreen.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:flutter_app/auth/forgotPwScreen.dart';
import 'package:flutter_app/auth/homeScreen.dart';

import 'package:flutter_app/src/signup.dart';
import 'package:flutter_app/src/Widget/bezierContainer.dart';

import 'package:flutter_app/viewmodel/loginViewModel.dart';

import 'package:flutter_app/comman/constant.dart';
import 'package:flutter_app/comman/app_themes.dart';
import 'package:flutter_app/comman/size_config.dart';
import 'package:flutter_app/comman/strings.dart';
import 'package:flutter_app/comman/validation_utils.dart';
import 'package:flutter_app/comman/custom_icons.dart';
import 'package:flutter_app/comman/custom_text_field.dart';
import 'package:flutter_app/comman/custom_button.dart';
import 'package:flutter_app/comman/commonClass.dart';
import 'package:flutter_app/comman/intel_utility.dart';

class LoginScreen extends StatefulWidget {
  static const String tag = "/login";

  LoginScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

final FirebaseAuth _auth = FirebaseAuth.instance;

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  String _email, _password;
  LoginViewModel viewModel = LoginViewModel();

  TextEditingController _emailController = TextEditingController();
  TextEditingController _pwdController = TextEditingController();

  Widget _topView() {
    return Container(
      height: 190.0,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            AppThemes.mainAppColor,
            AppThemes.redColor,
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: SizeConfig.safeBlockVertical * 3,
          ),
          Container(
            padding: EdgeInsets.only(
                top: 15.0, left: 10.0, right: 30.0, bottom: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Constant.isUserSkipLogin
                    ? IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          size: 30.0,
                          color: AppThemes.whiteColor,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      )
                    : Container(),
                Spacer(),
                InkWell(
                  onTap: () {
                    Constant.isUserSkipLogin = true;
                    //CommonClass.navigateAndRemoveNavigation(context, HomeScreen());
                    //Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));
                    IntelUtility.navigateReplaceToScreen(context, HomeScreen());
                  },
                  child: Container(
                    padding:
                        EdgeInsets.only(top: 15.0, left: 30.0, bottom: 10.0),
                    child: Row(
                      children: [
                        Text(
                          "Skip",
                          style: AppThemes.textStyleNormalWith(
                              fontSize: SizeConfig.safeBlockHorizontal * 4,
                              fontWeight: FontWeight.w500,
                              color: AppThemes.whiteColor,
                              decoration: TextDecoration.underline),
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          size: 10.0,
                          color: AppThemes.whiteColor,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          Container(
            padding: EdgeInsets.only(left: 30.0, right: 30.0, bottom: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Welcome to",
                  style: AppThemes.textStyleItalicWith(
                    fontSize: SizeConfig.safeBlockHorizontal * 5,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "Take Your Seat",
                  style: AppThemes.textStyleNormalWith(
                    fontSize: SizeConfig.safeBlockHorizontal * 6,
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _inputFields() {
    return Container(
      child: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: Column(
          children: [
            CustomTextField(
              color: AppThemes.lightGrayColor,
              hint: "abc@gmail.com",
              labelText: "Email",
              controller: _emailController,
              keyBoardType: TextInputType.emailAddress,
              onSaved: (value) => _email = value,
              validator: Validations.validateEmail,
              prefixWidget: CustomIcons.emailIcon,
            ),
            CustomTextField(
              obscure: true,
              maxLine: 1,
              color: AppThemes.lightGrayColor,
              hint: "Password",
              labelText: "Password",
              controller: _pwdController,
              keyBoardType: TextInputType.text,
              onSaved: (value) => _password = value,
              validator: (value) {
                if (value.isEmpty) {
                  return Strings.emptypassword;
                } else {
                  return null;
                }
              },
              prefixWidget: CustomIcons.passwordIcon,
            ),
          ],
        ),
      ),
    );
  }

  Widget _backButton() {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
              child: Icon(Icons.keyboard_arrow_left, color: Colors.black),
            ),
            Text('Back',
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _text.dispose();
    super.dispose();
  }

  final _text = TextEditingController();

  showAlertDialog(BuildContext context, String message) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed: () {
        //Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));
        IntelUtility.navigateReplaceToScreen(context, HomeScreen());
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Success"),
      content: Text(message),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showSuccessFailedAlertDialog(BuildContext context, String message) {
    // set up the buttons
    Widget continueButton = FlatButton(
      child: Text("Ok"),
      onPressed: () {
        //Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));
        Navigator.of(context).pop();
       // IntelUtility.navigateReplaceToScreen(context, HomeScreen());
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text(message),
      actions: [
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget _logInButton() {
    return InkWell(
      onTap: () {
        //showSuccessFailedAlertDialog(context, "Your Login Successfully.");
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 5),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [Color(0xfffbb448), Color(0xfffbb448)])),
        child: filledButton(
          text: "LOGIN",
          fontSize: SizeConfig.safeBlockHorizontal * 4,
          radius: 5.0,
          textColor: AppThemes.whiteColor,
          function: _validateInput,
        ),
      ),
    );
  }

  _validateInput() {
    final FormState form = _formKey.currentState;
    if (_formKey.currentState.validate()) {
      form.save();
      viewModel.email = _email;
      viewModel.password = _password;
      //doLoginApi();
      doFcmAuthLogin();
//      showAlertDialog(context);
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }


  doFcmAuthLogin(){
    IntelUtility.showLoader(true,context);
    _signInCall().then((pref) {
      IntelUtility.showLoader(false,context);
      if (pref != null) {
        if(pref =="Signed In successfully."){
          IntelUtility.navigateReplaceToScreen(context, HomeScreen());
        }else{
          showSuccessFailedAlertDialog(context,pref);
        }

      } else {
        print("else auth data----===-> " + pref);
        showSuccessFailedAlertDialog(context,pref);
      }
    });
  }

  Future<String> _signInCall({String email, String password}) async {
    try {
      await _auth.signInWithEmailAndPassword(
          email: _emailController.text, password: _pwdController.text);
      return "Signed In successfully.";
    } on FirebaseAuthException catch (e) {
      print('Failed message -------===========> '+e.message);
      if (e.code == 'weak-password') {
        return "The password provided is too weak.";
      } else if (e.code == 'email-already-in-use') {
        return "The account already exists for that email.";
      } else {
        return e.message;
      }
    } catch (e) {
      print(e);
    }
  }




  doLoginApi() {
    IntelUtility.showLoader(true, context);
    viewModel.performLogin((success, message) {
      print("Response Message login::::::---=-> " + message);
      IntelUtility.showLoader(false, context);
      if (success) {
        Constant.isUserSkipLogin = false;
        CommonClass.isUserLoggedIn = true;

        //Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomeScreen()), (Route<dynamic> route) => false,);

        IntelUtility.navigateReplaceToScreen(context, HomeScreen());
      } else {
        //IntelUtility.showNotificationAlert(message);
        showSuccessFailedAlertDialog(context, message);
      }
    });
  }

  Widget _divider() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          Text('or'),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }

  Widget _createAccountLabel() {
    return InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => SignUpPage("", "", "", 0)));
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        padding: EdgeInsets.all(15),
        alignment: Alignment.bottomCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Don\'t have an account ?',
              style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              'Register',
              style: TextStyle(
                  color: Color(0xfff79c4f),
                  fontSize: 13,
                  fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    );
  }

  Widget _ForgotPwLabel() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ForgotPwScreen()));
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        padding: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.centerRight,
        child: Text('Forgot Password ?',
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
      ),
    );
  }

  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'flu',
          style: GoogleFonts.portLligatSans(
            textStyle: Theme.of(context).textTheme.display1,
            fontSize: 30,
            fontWeight: FontWeight.w700,
            color: Color(0xffe46b10),
          ),
          children: [
            TextSpan(
              text: 'tt',
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
            TextSpan(
              text: 'er',
              style: TextStyle(color: Color(0xffe46b10), fontSize: 30),
            ),
          ]),
    );
  }

  Widget _logintitle() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'Login',
          style: GoogleFonts.portLligatSans(
            textStyle: Theme.of(context).textTheme.display1,
            fontSize: 30,
            fontWeight: FontWeight.w700,
            color: Color(0xffe46b10),
          )),
    );
  }

  //Set Edit text filed
  Widget _enteryFiled(String title, {bool isPassword = false}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
              //validator: (val) => val.isEmpty || !val.contains("@") ? "enter a valid eamil" : null,
              obscureText: isPassword,
              decoration: InputDecoration(
                  hintText: title,
                  border: InputBorder.none,
                  fillColor: Color(0xfff3f3f4),
                  filled: true))
        ],
      ),
    );
  }

  bool _validate = false;

  Widget _enteryEmailFiled(String title, {bool isPassword = false}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /* Form(
            autovalidate: true,
            child: TextFormField(
              validator: (value) => EmailValidator.validate(value) ? null : "Please enter a valid email",
            ),
          ),*/
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
              //validator: (val) => val.isEmpty || !val.contains("@") ? "enter a valid eamil" : null,
              obscureText: isPassword,
              decoration: InputDecoration(
                  hintText: title,
                  errorText: _validate ? 'Please enter email id.' : null,
                  border: InputBorder.none,
                  fillColor: Color(0xfff3f3f4),
                  filled: true))
        ],
      ),
    );
  }

  Widget _emailPasswordWidget() {
    return Column(
      children: <Widget>[
        _enteryEmailFiled("Email Id"),
        _enteryFiled("Password", isPassword: true),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      onTap: () {
        CommonClass.hideKeyBoard(context);
      },
      child: Scaffold(
        backgroundColor: AppThemes.whiteColor,
        body: SingleChildScrollView(
          child: Column(
            children: [
              _topView(),
              Container(
                margin: EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _inputFields(),
                    _ForgotPwLabel(),
                    SizedBox(
                      height: SizeConfig.safeBlockVertical * 1,
                    ),
                    _logInButton(),
                    SizedBox(
                      height: SizeConfig.safeBlockVertical * 1,
                    ),
                    _createAccountLabel(),
                    SizedBox(
                      height: SizeConfig.safeBlockVertical * 1,
                    ),
                    RaisedButton(
                      child: Text('Sam Login'),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SamLoginScreen()));
                      },
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
