import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/auth/homeBottomPage.dart';
import 'package:flutter_app/auth/messageBottomPage.dart';
import 'package:flutter_app/auth/profile/instagramStories.dart';
import 'package:flutter_app/auth/profileBottomPage.dart';
import 'package:flutter_app/auth/sam/homeBottomBar.dart';
import 'package:flutter_app/auth/sam/materialToolBarScreen.dart';
import 'package:flutter_app/auth/sam/searchListData.dart';
import 'package:flutter_app/auth/sam/videoViewScreen.dart';
import 'package:flutter_app/auth/tabview/viewPagerScreen.dart';
import 'package:flutter_app/auth/timelineVw/timelineComponent.dart';
import 'package:flutter_app/auth/userAuthScreen.dart';
import 'package:flutter_app/auth/sam/chatBottomPage.dart';
import 'package:flutter_app/comman/intel_utility.dart';
import 'package:flutter_app/auth/timelineVw/timelineView.dart';

class NewHomeScreen extends StatefulWidget {
  @override
  _NewHomeScreenState createState() => _NewHomeScreenState();
}

class _NewHomeScreenState extends State<NewHomeScreen> {
  var _currentIndex = 0;
  String _title;
  final List<Widget> _children = [
    HomeBottomBar(), //index=0
    MessageBottomPage(), //index=1
    MaterialToolBarScreen(), //index=2
    ViewPagerScreen(), //index=4
  ];

  @override
  initState() {
    _title = 'Some default value';
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        endDrawerEnableOpenDragGesture: false,
        // This way it will not open
        key: _scaffoldKey,
        appBar: AppBar(
          flexibleSpace: Image(
            image: AssetImage("assets/images/header_bg.png"),
            fit: BoxFit.cover,
          ),
          backgroundColor: Colors.transparent,
          title: Text(
            'Home',
            style: TextStyle(
              fontSize: 14,
              color: Colors.green,
            ),
          ),
          leading: Builder(
            builder: (context) => // Ensure Scaffold is in context
                IconButton(
              icon: Icon(
                Icons.menu,
                color: Colors.green,
              ),
              onPressed: () => _scaffoldKey.currentState.openDrawer(),
            ),
          ),
          actions: [
            CircleAvatar(
              radius: 25.0,
              backgroundColor: const Color(0xFF778899),
              backgroundImage:
                  NetworkImage("http://tineye.com/images/widgets/mona.jpg"),
            ),
          ],
        ),
        //endDrawer: AppDrawer(),
        body: _children[_currentIndex],
        drawer: _slideMenuDrawer(),
        bottomNavigationBar: _bottomNavigationView(),
      ),
    );
  }

  //Slide Menu
  Widget _slideMenuDrawer() {
    return Drawer(
      elevation: 5,
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountEmail: Text(
              'Mujju@gmail.com',
              style: TextStyle(
                fontSize: 16,
                color: Colors.deepPurple,
              ),
            ),
            accountName: Row(
              children: [
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(shape: BoxShape.circle),
                  //Todo: Assets image set in drawer layout
                  child: CircleAvatar(
                    radius: 50.0,
                    backgroundColor: const Color(0xFF778899),
                    backgroundImage: NetworkImage(
                        "http://tineye.com/images/widgets/mona.jpg"),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Muzammil Modan',
                      style: TextStyle(
                        fontSize: 13,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      'mujjuRaja@gmail.com',
                      style: TextStyle(
                        fontSize: 13,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          ListTile(
            title: Text("Video View"),
            onTap: () {
              IntelUtility.navigatePushToScreen(context, VideoViewScreen());
            },
          ),
          ListTile(
            title: Text("Time line View"),
            onTap: () {
              IntelUtility.navigateReplaceToScreen(context, TimelineView());
            },
          ),
          ListTile(
            title: Text("Instagram Stories"),
            onTap: () {
              IntelUtility.navigateReplaceToScreen(context, InstagramStories());
            },
          ),

        ],
      ),
    );
  }

  //Bottom Navigation
  Widget _bottomNavigationView() {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/images/home_bottom_bg.png"),
              fit: BoxFit.fill)),
      child: BottomNavigationBar(
        onTap: onTabTapped,
        elevation: 5.0,
        //Todo: Set background color as per requiredment
        //backgroundColor: Colors.greenAccent,
        currentIndex: _currentIndex,
        selectedItemColor: Theme.of(context).primaryColor,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              title: Text('Home')),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.add_shopping_cart_outlined,
              ),
              title: Text('My Ads')),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.chat,
              ),
              title: Text('Chat')),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.supervised_user_circle_rounded,
              ),
              title: Text('View pager')),
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
      switch (index) {
        case 0:
          {
            _title = 'Home';
          }
          break;

        case 1:
          {
            _title = 'My Ads';
          }
          break;

        case 2:
          {
            _title = 'Chat';
          }
          break;

        case 3:
          {
            _title = 'Profile';
          }
          break;
       /* case 4:
          {
            _title = 'View pager';
          }
          break;*/
      }
    });
  }
}
