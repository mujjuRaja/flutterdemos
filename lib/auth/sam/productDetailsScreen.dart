import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app/comman/color_extenstion.dart';
import 'package:flutter_app/model/itemListData.dart';
import 'package:flutter_app/comman/dotsIndicator.dart';
import 'package:worm_indicator/shape.dart';
import 'package:worm_indicator/worm_indicator.dart';

class ProductDetailsScreen extends StatefulWidget {
  @override
  _ProductDetailsScreenState createState() => _ProductDetailsScreenState();
}

class _ProductDetailsScreenState extends State<ProductDetailsScreen> {
  PageController _controller;

  @override
  void initState() {
    super.initState();
    _controller = PageController();
  }

  Widget buildPageView() {
    return PageView.builder(
      physics: AlwaysScrollableScrollPhysics(),
      controller: _controller,
      itemBuilder: (BuildContext context, int pos) {
        return Container(
          margin: EdgeInsets.all(20),
          child: Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Image.asset(
                ItemListData.listCategoryIcons[pos],
                width: double.infinity,
                height: 150,
              )
            ],
          ),
        );
      },
      itemCount: ItemListData.listCategoryIcons.length,
    );
  }

  Widget buildExampleIndicatorWithShapeAndBottomPos(
      Shape shape, double bottomPos) {
    return Positioned(
      bottom: bottomPos,
      left: 0,
      right: 0,
      child: WormIndicator(
        length: ItemListData.listCategoryIcons.length,
        controller: _controller,
        //shap= circleShape/squareShape/rectangleShape... As per changed as required shap
        shape: shape,
      ),
    );
  }

//Todo: Create shape
  final circleShape = Shape(
    size: 14,
    shape: DotShape.Circle,
    spacing: 8,
  );
  final squareShape = Shape(
    size: 16,
    shape: DotShape.Square,
    spacing: 8,
  );
  final rectangleShape = Shape(
    width: 16,
    height: 24,
    shape: DotShape.Rectangle,
    spacing: 8,
  );

  Widget _topProductDetails() {
    return Container(
      padding: EdgeInsets.only(right: 15, left: 15, top: 10, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Product Title',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 16,
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.italic,
            ),
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            'Product sub Title',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 12,
              color: Colors.grey,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            'orem Ipsum is simply dummy text of the printing and typesetting industry.'
            ' Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,'
            ' when an unknown printer took a galley of type and scrambled it to make a type specimen book',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 10,
              color: Colors.grey,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 2,
          ),
          Row(
            children: [
              RichText(
                textAlign: TextAlign.left,
                text: TextSpan(
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: "\$50000   ",
                      style: TextStyle(
                        color: Colors.green,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    TextSpan(
                        text: "Price/Month",
                        style: TextStyle(
                          color: Colors.black,
                        ))
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _sellerDetails() {
    return Container(
      child: Card(
        margin: EdgeInsets.only(right: 15,left: 15,top: 10,bottom: 10),
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Container(
          //padding: EdgeInsets.all(10.0),
          height: 120,

          //Border set
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.blue,
            ),
            borderRadius: BorderRadius.circular(10.0),
          ),

          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                child:Image.asset(
                  "assets/images/user.png",
                  height: 100.0,
                  width: 100.0,
                  // alignment: Alignment.topLeft,
                  //fit: BoxFit.fitHeight,
                ),
              ),
              Flexible(
                //padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                child: Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Mujju Raja",
                        textAlign: TextAlign.left,
                        //overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Color(0xFFD73C29),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        'abc@gmail.com',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.black54,
                          fontSize: 9.0,
                        ),
                      ),
                      Text(
                        '1234567890',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.black54,
                          fontSize: 9.0,
                        ),
                      ),
                      Text(
                        'Ahamdabad,382150',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.black54,
                          fontSize: 9.0,
                        ),
                      ),
                      //GetRatings(),
                      SizedBox(height: 2.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 4.0),
                            child: Column(
                              children: <Widget>[
                                Text(
                                  'Join DATE:',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    color: Colors.black38,
                                    fontSize: 9.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  "27/7/2021",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 9.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 4.0),
                            child: Column(
                              children: <Widget>[
                                Text(
                                  'Birth Date:',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    color: Colors.black38,
                                    fontSize: 9.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  '07/08/1992',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 9.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Product Details',
          style: TextStyle(
            fontSize: 14,
            color: Colors.white,
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            _viewPagerView(),
            Divider(
              height: 2,
              color: Colors.green,
            ),
            _topProductDetails(),
            Divider(
              height: 2,
              color: Colors.green,
            ),
            _sellerDetails()
          ],
        ),
      ),
    );
  }

  Widget _viewPagerView() {
    return Container(
      width: double.infinity,
      height: 150,
      child: Stack(
        children: <Widget>[
          buildPageView(),
          buildExampleIndicatorWithShapeAndBottomPos(circleShape, 10),
        ],
      ),
    );
  }
}
