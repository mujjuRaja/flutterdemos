import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shape_of_view/shape_of_view.dart';

class VideoViewScreen extends StatefulWidget {
  @override
  _VideoViewScreenState createState() => _VideoViewScreenState();
}

class _VideoViewScreenState extends State<VideoViewScreen> {
  // VideoPlayerController _videoPlayerController;
  final videoUrl =
      "https://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_20mb.mp4";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: 20,right: 15,left: 15,),
          child: Column(
            children: <Widget>[
              _RoundRectShape(),
              SizedBox(height: 10,),
              _ArcRectShape(),
              SizedBox(height: 10,),
              _DiagonalShape(),
              SizedBox(height: 10,),
              _BubbleShape(),
              SizedBox(height: 10,),
              _PolygonShape(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _RoundRectShape() {
    return ShapeOfView(
      height: 150,
      shape: RoundRectShape(
        borderRadius: BorderRadius.circular(12),
        borderColor: Colors.white, //optional
        borderWidth: 2, //optional
      ),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                'https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/4bb82b72535211.5bead62fe26d5.jpg'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Widget _ArcRectShape() {
    return ShapeOfView(
      height: 150,
      shape: ArcShape(
          direction: ArcDirection.Outside,
          height: 20,
          position: ArcPosition.Bottom
      ),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                'https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/4bb82b72535211.5bead62fe26d5.jpg'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Widget _DiagonalShape(){
    return ShapeOfView(
      height: 150,
        elevation: 4,
        shape: DiagonalShape(
            position: DiagonalPosition.Bottom,
            direction: DiagonalDirection.Right,
            angle: DiagonalAngle.deg(angle: 10)
        ),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                'https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/4bb82b72535211.5bead62fe26d5.jpg'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Widget _BubbleShape(){
    return ShapeOfView(
      height: 150,
      elevation: 4,
      shape: BubbleShape(
          position: BubblePosition.Bottom,
          arrowPositionPercent: 0.5,
          borderRadius: 20,
          arrowHeight: 10,
          arrowWidth: 10
      ),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                'https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/4bb82b72535211.5bead62fe26d5.jpg'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Widget _PolygonShape(){
    return ShapeOfView(
      height: 150,
      elevation: 4,
      shape: PolygonShape(
          numberOfSides: 9,

      ),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                'https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/4bb82b72535211.5bead62fe26d5.jpg'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }


}
