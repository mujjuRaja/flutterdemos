import 'dart:math';

import 'package:flutter/material.dart';

class MaterialToolBarScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MaterialToolBarScreenState();
}

class _MaterialToolBarScreenState extends State<MaterialToolBarScreen> {
  final List<String> listItems = [];

  final List<String> _tabs = <String>[
    "Featured",
    "Popular",
    "Latest",
  ];

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        body: DefaultTabController(
          length: _tabs.length, // This is the number of tabs.
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              // These are the slivers that show up in the "outer" scroll view.
              return <Widget>[
                SliverOverlapAbsorber(
                  handle:
                      NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                  sliver: SliverSafeArea(
                    top: false,
                    sliver: SliverAppBar(
                      title: const Text(
                        'Materials header',
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      ),
                      floating: true,
                      pinned: true,
                      snap: false,
                      primary: true,
                      //Todo: top header size
                      expandedHeight: MediaQuery.of(context).size.height * 0.3,
                      // backgroundColor: const Color(0xff1c0436),
                      //Todo: left side Icons set
                      //leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: () {}),
                      //Todo: Right side icons set
                      actions: [
                        IconButton(
                            icon: Icon(Icons.more_vert), onPressed: () {})
                      ],
                      forceElevated: innerBoxIsScrolled,
                      bottom: TabBar(
                        // These are the widgets to put in each tab in the tab bar.
                        tabs: _tabs
                            .map((String name) => Tab(text: name))
                            .toList(),
                      ),
                      //Todo: set custom view...
                      flexibleSpace: Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: NetworkImage(
                                'https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/4bb82b72535211.5bead62fe26d5.jpg'),
                            //your image
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.vertical(
                            //image rounded radius set
                            top: Radius.circular(0),
                            bottom: Radius.circular(0),
                          ),
                        ),
                        /*child: FlexibleSpaceBar(
                          collapseMode: CollapseMode.pin,
                          centerTitle: true,
                          title: Text('A Synthwave Mix'),
                        ),*/
                      ),
                    ),
                  ),
                ),
              ];
            },
            body: TabBarView(
              // These are the contents of the tab views, below the tabs.
              children: _tabs.map((String name) {
                return SafeArea(
                  top: false,
                  bottom: false,
                  child: Builder(
                    builder: (BuildContext context) {
                      return CustomScrollView(
                        key: PageStorageKey<String>(name),
                        slivers: <Widget>[
                          SliverOverlapInjector(
                            handle:
                                NestedScrollView.sliverOverlapAbsorberHandleFor(
                                    context),
                          ),
                          SliverPadding(
                            padding: const EdgeInsets.all(8.0),
                            sliver: SliverFixedExtentList(
                              itemExtent: 60.0,
                              delegate: SliverChildBuilderDelegate(
                                (BuildContext context, int index) {
                                  return Container(
                                      color: Color(
                                              (Random().nextDouble() * 0xFFFFFF)
                                                      .toInt() <<
                                                  0)
                                          .withOpacity(1.0));
                                },
                                childCount: 30,
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                );
              }).toList(),
            ),
          ),
        ),
      ),
    );
  }
}
