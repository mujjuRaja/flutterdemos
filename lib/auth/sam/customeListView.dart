import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/auth/sam/searchListData.dart';
import 'package:flutter_app/comman/intel_utility.dart';
import 'package:pagination_view/pagination_view.dart';
import 'package:paging/paging.dart';

class CustomeListView extends StatefulWidget {
  @override
  _CustomeListViewState createState() => _CustomeListViewState();
}

class _CustomeListViewState extends State<CustomeListView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text(
          "List with search",
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              IntelUtility.navigatePushToScreen(context, SearchListData());
            },
          )
        ],
        backgroundColor: Colors.green,
      ),
      body: Pagination<String>(
        pageBuilder: (currentSize) => pageStaticListData(currentSize),
        itemBuilder: (index, item) {
          return Container(
            color: Colors.grey,
            height: 48,
            child: Text(item),
          );
        },
      ),
    );
  }

  static const int _COUNT = 5;

  // mocking a network call
  Future<List<String>> pageStaticListData(int previousCount) async {
    await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
    List<String> dummyList = List();
    if (previousCount < 30) {
      // stop loading after 30 items
      for (int i = previousCount; i < previousCount + _COUNT; i++) {
        dummyList.add('Item $i');
      }
    }
    return dummyList;
  }

  Widget _CardViewData() {
    return Container(
      child: Card(
        margin: EdgeInsets.only(right: 15, left: 15, top: 10),
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Container(
          padding: EdgeInsets.all(10.0),
          height: 140,

          //Border set
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.blue,
            ),
            borderRadius: BorderRadius.circular(10.0),
          ),

          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.asset(
                "assets/images/ic_more_app.png",
                height: 120.0,
                width: 120.0,
                // alignment: Alignment.topLeft,
                //fit: BoxFit.fitHeight,
              ),
              Flexible(
                //padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                child: Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Title",
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Color(0xFFD73C29),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        'Category data',
                        style: TextStyle(
                          color: Colors.black54,
                          fontSize: 9.0,
                        ),
                      ),
                      SizedBox(height: 0.0),
                      //GetRatings(),
                      SizedBox(height: 2.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 4.0),
                            child: Column(
                              children: <Widget>[
                                Text(
                                  'RELEASE DATE:',
                                  style: TextStyle(
                                    color: Colors.black38,
                                    fontSize: 9.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  "27/7/2021",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 9.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 4.0),
                            child: Column(
                              children: <Widget>[
                                Text(
                                  'RUNTIME:',
                                  style: TextStyle(
                                    color: Colors.black38,
                                    fontSize: 9.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  'Title',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 9.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
