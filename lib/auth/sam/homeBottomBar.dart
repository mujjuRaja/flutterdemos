import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/auth/sam/customeListView.dart';
import 'package:flutter_app/comman/intel_utility.dart';
import 'package:flutter_app/comman/size_config.dart';
import 'package:flutter_app/model/itemListData.dart';
import 'package:flutter_app/auth/sam/productDetailsScreen.dart';

class HomeBottomBar extends StatefulWidget {
  @override
  _HomeBottomBarState createState() => _HomeBottomBarState();
}

Widget _verticelListView(BuildContext context) {
  bool isSelected = false;

  return Expanded(
    child: ListView.builder(
      //Todo : means Set Row list set pedding like start and end side
      padding: const EdgeInsets.all(5),
      itemCount: ItemListData.listTitles.length,
      itemBuilder: (context, index) {
        final item = ItemListData.listTitles[index];
        return Dismissible(
          key: Key(ItemListData.listTitles[index]),
          child: Card(
            //Set Card view Selected or not
            shape: isSelected
                ? new RoundedRectangleBorder(
                    side: new BorderSide(color: Colors.blue, width: 2.0),
                    borderRadius: BorderRadius.circular(4.0))
                : new RoundedRectangleBorder(
                    side: new BorderSide(color: Colors.white, width: 2.0),
                    borderRadius: BorderRadius.circular(4.0)),

            child: ListTile(
              onTap: () {
                IntelUtility.navigatePushToScreen(
                    context, ProductDetailsScreen());
                //isSelected = true;
              },
              title: Align(
                child: new Text(item),
                alignment: Alignment(1, 0),
              ),
              leading: Image.asset(
                "assets/images/bike_blc.jpg",
                width: 40.0,
                height: 40.0,
              ),
            ),
          ),
        );
      },
    ),
  );
}

//final List<int> numbers = [1, 2, 3, 5, 8, 13, 21, 34, 55];

Widget _horizontalListView(BuildContext context) {
  return Container(
    padding: EdgeInsets.all(1),
    height: 120,
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: ItemListData.listCategory.length,
      itemBuilder: (context, index) {
        return Container(
          child: Stack(
            children: <Widget>[
              InkWell(
                onTap: (){
                  IntelUtility.navigatePushToScreen(context, CustomeListView());
                },

                child: Container(
                  alignment: Alignment.center,
                  child: Image.asset(
                    "assets/images/ic_home_cate.png",
                    width: 155.0,
                    height: 100.0,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.bottomCenter,
                padding: EdgeInsets.only(left: 62.0, bottom: 25.0),
                child: Text(
                  ItemListData.listCategory[index],
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 14.0),
                ),
              ),
              Container(
                alignment: Alignment.topCenter,
                padding: EdgeInsets.only(top: 15, left: 50),
                child: Image.asset(
                  ItemListData.listCategoryIcons[index].toString(),
                  width: 50.0,
                  height: 50.0,
                ),
              ),
            ],
          ),
        );
      },
    ),
  );
}

Widget _myListView(BuildContext context) {
  return Container(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: SizeConfig.safeBlockVertical * 3,
        ),
        Text(
          'Category',
          textAlign: TextAlign.start,
          style: TextStyle(
            fontSize: 14,
            fontStyle: FontStyle.italic,
            color: Colors.black,
          ),
        ),
        SizedBox(
          height: SizeConfig.safeBlockVertical * 2,
        ),
        _horizontalListView(context),
        SizedBox(
          height: SizeConfig.safeBlockVertical * 3,
        ),
        Text(
          'Recommand List',
          textAlign: TextAlign.left,
          style: TextStyle(
            fontSize: 14,
            fontStyle: FontStyle.italic,
            color: Colors.black,
          ),
        ),
        SizedBox(
          height: SizeConfig.safeBlockVertical * 2,
        ),
        _verticelListView(context),
      ],
    ),
  );
}

class _HomeBottomBarState extends State<HomeBottomBar> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      body: new Builder(
        builder: (BuildContext context) {
          return _myListView(context);
        },
      ),
    );
  }
}
