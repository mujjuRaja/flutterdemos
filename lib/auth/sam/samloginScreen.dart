import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/auth/forgotPwScreen.dart';
import 'package:flutter_app/auth/homeScreen.dart';
import 'package:flutter_app/auth/sam/newHomeScreen.dart';
import 'package:flutter_app/auth/signUpScreen.dart';
import 'package:flutter_app/comman/app_themes.dart';
import 'package:flutter_app/comman/commonClass.dart';
import 'package:flutter_app/comman/constant.dart';
import 'package:flutter_app/comman/custom_icons.dart';
import 'package:flutter_app/comman/custom_text_field.dart';
import 'package:flutter_app/comman/intel_utility.dart';
import 'package:flutter_app/comman/size_config.dart';

import 'package:flutter_app/comman/strings.dart';
import 'package:flutter_app/comman/validation_utils.dart';
import 'package:flutter_app/comman/custom_button.dart';
import 'package:flutter_app/comman/pref_utils.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_app/comman/loading.dart';
import 'package:flutter_app/auth/otp/mobileNoLoginScreen.dart';

class SamLoginScreen extends StatefulWidget {
  @override
  _SamLoginScreenState createState() => _SamLoginScreenState();
}

class _SamLoginScreenState extends State<SamLoginScreen> {
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  SharedPreferences prefs;

  bool isLoading = false;
  bool isLoggedIn = false;
  User currentUser;

  @override
  void initState() {
    super.initState();
    isSignedIn();
  }

  void isSignedIn() async {
    this.setState(() {
      isLoading = true;
    });

    prefs = await SharedPreferences.getInstance();

    isLoggedIn = await googleSignIn.isSignedIn();

    if (isLoggedIn) {
      Preferences.setString(Preferences.fcmId, prefs.getString('id'));
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HomeScreen()),
      );
    }

    this.setState(() {
      isLoading = false;
    });
  }

  Future<Null> handleSignIn() async {
    prefs = await SharedPreferences.getInstance();

    this.setState(() {
      isLoading = true;
    });

    GoogleSignInAccount googleUser = await googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    User firebaseUser =
        (await firebaseAuth.signInWithCredential(credential)).user;

    if (firebaseUser != null) {
      // Check is already sign up
      final QuerySnapshot result = await FirebaseFirestore.instance
          .collection('users')
          .where('id', isEqualTo: firebaseUser.uid)
          .get();
      final List<DocumentSnapshot> documents = result.docs;
      if (documents.length == 0) {
        // Update data to server if new user
        FirebaseFirestore.instance
            .collection('users')
            .doc(firebaseUser.uid)
            .set({
          'nickname': firebaseUser.displayName,
          'photoUrl': firebaseUser.photoURL,
          'id': firebaseUser.uid,
          'createdAt': DateTime.now().millisecondsSinceEpoch.toString(),
          'chattingWith': null
        });

        // Write data to local
        currentUser = firebaseUser;
        await prefs.setString('id', currentUser.uid);
        await prefs.setString('nickname', currentUser.displayName);
        await prefs.setString('photoUrl', currentUser.photoURL);
      } else {
        // Write data to local
        await prefs.setString('id', documents[0].data()['id']);
        await prefs.setString('nickname', documents[0].data()['nickname']);
        await prefs.setString('photoUrl', documents[0].data()['photoUrl']);
        await prefs.setString('aboutMe', documents[0].data()['aboutMe']);
      }
      Fluttertoast.showToast(msg: "Sign in success");
      this.setState(() {
        isLoading = false;
      });

      Preferences.setString(Preferences.fcmId, firebaseUser.uid);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
    } else {
      Fluttertoast.showToast(msg: "Sign in fail");
      this.setState(() {
        isLoading = false;
      });
    }
  }

  Widget _topViews() {
    return Container(
      height: 190.0,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: <Color>[AppThemes.mainAppColor, AppThemes.mainAppColor]),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: SizeConfig.safeBlockVertical * 3),
          Container(
            padding: EdgeInsets.only(
                top: 15.0, bottom: 10.0, right: 30.0, left: 10.0),
            child: Row(
              children: [
                Constant.isUserSkipLogin
                    ? IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          size: 16,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      )
                    : Container(),
                Spacer(),
                InkWell(
                  onTap: () {
                    Constant.isUserSkipLogin = true;
                    IntelUtility.navigateReplaceToScreen(
                        context, NewHomeScreen());
                    Preferences.setBool(Preferences.isLogin, true);
                  },
                  child: Container(
                    padding:
                        EdgeInsets.only(top: 15.0, left: 30.0, bottom: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Skip",
                          style: AppThemes.textStyleNormalWith(
                              fontSize: SizeConfig.safeBlockHorizontal * 4,
                              fontWeight: FontWeight.w500,
                              color: AppThemes.whiteColor,
                              decoration: TextDecoration.underline),
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          size: 10.0,
                          color: AppThemes.whiteColor,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          Container(
            padding: EdgeInsets.only(bottom: 15.0, right: 20.0, left: 30.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Welcome to',
                      style: AppThemes.textStyleNormalWith(
                        fontSize: SizeConfig.safeBlockHorizontal * 6,
                        color: AppThemes.blackColor,
                      ),
                    ),
                    Text(
                      'Flutter app',
                      style: AppThemes.textStyleNormalWith(
                          fontSize: SizeConfig.safeBlockHorizontal * 4,
                          color: AppThemes.whiteColor,
                          fontWeight: FontWeight.w400),
                    )
                  ],
                ),
                //SizedBox(height: SizeConfig.safeBlockVertical * 1,)
                SizedBox(
                  width: SizeConfig.safeBlockVertical * 1,
                ),
                Text(
                  'Mujju Raja',
                  style: AppThemes.textStyleNormalWith(
                    fontSize: SizeConfig.safeBlockHorizontal * 6,
                    color: AppThemes.darkGrayColor,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  String _email, _password;
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  bool _showPassword = false;

  void _togglevisibility() {
    setState(() {
      _showPassword = !_showPassword;
    });
  }

  Widget _inputFiledView() {
    return Container(
      margin: EdgeInsets.only(top: 35.0, right: 15.0, left: 15.0),
      child: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: Column(
          children: [
            CustomTextField(
              color: AppThemes.redColor,
              hint: "test@gmail.com",
              labelText: Strings.emailTtl,
              controller: _emailController,
              keyBoardType: TextInputType.emailAddress,
              validator: Validations.validateEmail,
              prefixWidget: CustomIcons.emailIcon,
              onSaved: (value) => _email = value,
            ),
            SizedBox(
              height: SizeConfig.safeBlockVertical * 1,
            ),
            CustomTextField(
              color: AppThemes.darkGrayColor,
              hint: "12354678",
              maxLine: 1,
              obscure: true,
              labelText: Strings.passwordTtl,
              decoration: InputDecoration(
                hintText: "Password",
                border: InputBorder.none,
                suffixIcon: GestureDetector(
                  onTap: () {
                    _togglevisibility();
                  },
                  child: Icon(
                    _showPassword ? Icons.visibility : Icons.visibility_off,
                    color: Colors.red,
                  ),
                ),
              ),
              controller: _passwordController,
              keyBoardType: TextInputType.visiblePassword,
              prefixWidget: CustomIcons.passwordIcon,
              onSaved: (value) => _password = value,
              validator: (value) {
                if (value.isEmpty) {
                  return Strings.emptypassword;
                } else {
                  return null;
                }
              },
            ),
            SizedBox(
              height: SizeConfig.safeBlockVertical * 2,
            ),
            /*TextFormField(
              controller: _passwordController,
              obscureText: !_showPassword,
              cursorColor: Colors.red,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                hintText: "Password",
                border: InputBorder.none,
                suffixIcon: GestureDetector(
                  onTap: () {
                    _togglevisibility();
                  },
                  child: Icon(
                    _showPassword ? Icons.visibility : Icons
                        .visibility_off, color: Colors.red,),
                ),
              ),
            ),*/
          ],
        ),
      ),
    );
  }

  Widget _forgotPasswordView() {
    return InkWell(
      onTap: () {
        print('Forgot clicked----------=-->');
        IntelUtility.navigateReplaceToScreen(context, ForgotPwScreen());
      },
      child: Container(
        margin: EdgeInsets.only(right: 25.0, left: 25.0),
        padding: EdgeInsets.symmetric(vertical: 5),
        alignment: Alignment.centerRight,
        child: Text(
          Strings.forgotPwTtl,
          style: TextStyle(
            fontSize: 16,
            color: AppThemes.mainAppColor,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }

  Widget _signInButtonView() {
    return InkWell(
      onTap: () {
        print('Signin clicked----------=-->');
        IntelUtility.navigateReplaceToScreen(context, NewHomeScreen());
        Preferences.setBool(Preferences.isLogin, true);
        //showSuccessFailedAlertDialog(context, "Your Login Successfully.");
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(right: 25.0, left: 25.0),
        padding: EdgeInsets.symmetric(vertical: 5),
        alignment: Alignment.center,
        /*decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.greenAccent,
              offset: Offset(2, 4),
              blurRadius: 5,
              spreadRadius: 2,
            )
          ],
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [Color(0xfffbb448), Color(0xfffbb448)],
          ),
        ),*/
        child: filledButton(
          text: "LOGIN",
          fontSize: SizeConfig.safeBlockHorizontal * 4,
          radius: 5.0,
          textColor: AppThemes.whiteColor,
          function: _validateInput,
        ),
      ),
    );
  }

  _validateInput() {
    final FormState form = _formKey.currentState;
    if (_formKey.currentState.validate()) {
      form.save();
/*      viewModel.email = _email;
      viewModel.password = _password;
      doLoginApi();*/
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }

  Widget _createNewAccountView() {
    return InkWell(
      onTap: () {
        IntelUtility.navigateReplaceToScreen(context, SignUpScreen());
      },
      child: Container(
        margin: EdgeInsets.symmetric(
          vertical: 15,
        ),
        padding: EdgeInsets.all(10.0),
        alignment: Alignment.centerLeft,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Don\'t have a account?',
              style: TextStyle(
                  fontSize: 15,
                  color: AppThemes.blackColor,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              width: SizeConfig.safeBlockVertical * 2,
            ),
            Text(
              'Registe Now',
              style: TextStyle(
                  fontSize: 16,
                  color: AppThemes.mainAppColor,
                  fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return GestureDetector(
        onTap: () {
          CommonClass.hideKeyBoard(context);
        },
        child: Scaffold(
          backgroundColor: AppThemes.whiteColor,
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                _topViews(),
                Container(
                  margin: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      _inputFiledView(),
                      _forgotPasswordView(),
                      SizedBox(
                        height: SizeConfig.safeBlockVertical * 1,
                      ),
                      _signInButtonView(),
                      SizedBox(
                        height: SizeConfig.safeBlockVertical * 1,
                      ),
                      _createNewAccountView(),
                      SizedBox(
                        height: SizeConfig.safeBlockVertical * 1,
                      ),
                      _googleWithLogin(),
                      SizedBox(
                        height: SizeConfig.safeBlockVertical * 1,
                      ),
                      _mobileNoWithLogin(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  Widget _mobileNoWithLogin() {
    return InkWell(
      onTap: () {
        print('Signin clicked----------=-->');
        IntelUtility.navigatePushToScreen(context, MobileNoLoginScreen());
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(right: 40.0, left: 40.0),
        padding: EdgeInsets.symmetric(vertical: 5),
        alignment: Alignment.center,
        child: filledButton(
          text: "Login with Mobile No",
          fontSize: SizeConfig.safeBlockHorizontal * 4,
          radius: 5.0,
          textColor: AppThemes.whiteColor,
        ),
      ),
    ); /*Container(
      child: InkWell(
        onTap: () {
          IntelUtility.navigatePushToScreen(context, MobileNoLoginScreen());
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.only(right: 25.0, left: 25.0),
          padding: EdgeInsets.symmetric(vertical: 5),
          alignment: Alignment.center,

          child: filledButton(
            text: 'Login with Mobile No',
            fontSize: 14,
            textColor: Colors.red,
          ),
        ),
      ),
    );*/
  }

  Widget _googleWithLogin() {
    return Container(
      child: Stack(
        children: <Widget>[
          Center(
            child: FlatButton(
                onPressed: () => handleSignIn().catchError((err) {
                      Fluttertoast.showToast(msg: "Sign in fail");
                      this.setState(() {
                        isLoading = false;
                      });
                    }),
                child: Text(
                  'SIGN IN WITH GOOGLE',
                  style: TextStyle(fontSize: 16.0),
                ),
                color: Color(0xffdd4b39),
                highlightColor: Color(0xffff7f7f),
                splashColor: Colors.transparent,
                textColor: Colors.white,
                padding: EdgeInsets.fromLTRB(30.0, 15.0, 30.0, 15.0)),
          ),

          // Loading
          Positioned(
            child: isLoading ? const Loading() : Container(),
          ),
        ],
      ),
    );
  }
}
