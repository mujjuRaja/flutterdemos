import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/auth/listItemDetailsScreen.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:share/share.dart';
import '../model/itemListData.dart';

class HomeBottomPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeBottomPageState();
  }
}

class HomeBottomPageState extends State<HomeBottomPage> {
//  TextEditingController controller = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  Widget slideRightBackground() {
    return Container(
      color: Colors.green,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: 20,
            ),
            Icon(
              Icons.edit,
              color: Colors.white,
            ),
            Text(
              " Edit",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ],
        ),
        alignment: Alignment.centerLeft,
      ),
    );
  }

  Widget slideLeftBackground() {
    return Container(
      color: Colors.red,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.delete,
              color: Colors.white,
            ),
            Text(
              " Delete",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.right,
            ),
            SizedBox(
              width: 20,
            ),
          ],
        ),
        alignment: Alignment.centerRight,
      ),
    );
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    ItemListData.listTitles
        .add((ItemListData.listTitles.length + 1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  Widget _myListView() {
    return ListView.builder(
      //Todo : means Set Row list set pedding like start and end side
      padding: const EdgeInsets.all(5),
      itemCount: ItemListData.listTitles.length,
      itemBuilder: (context, index) {
        final item = ItemListData.listTitles[index];
        return Dismissible(
          key: Key(ItemListData.listTitles[index]),
          child: Card(
            //Todo : means Set under Row list set pedding
            // padding: const EdgeInsets.all(10.0),
            child: ListTile(
              title: Text(item),
              leading: Icon(ItemListData.listIcons[index]),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    icon: const Icon(Icons.edit),
                    onPressed: () {
                      showEditDeleteAlert(context, index,
                          "Are you sure you want to Edit this item?");
                    },
                  ),
                  IconButton(
                    icon: const Icon(Icons.delete_forever),
                    onPressed: () {
                      showEditDeleteAlert(context, index,
                          "Are you sure you want to delete this item?");
                    },
                  ),
                  IconButton(
                    icon: const Icon(Icons.share),
                    onPressed: () {
                      // final RenderBox box = context.findRenderObject();
                      Share.share('Hello this is a test',
                          sharePositionOrigin: Rect.fromLTWH(10, 10, 10, 10));
                      //_onShareData(context);
                    },
                  ),
                ],
              ),
              onTap: () {
                //                                  <-- onTap
                setState(() {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    print("Clicked Pass Data ---====-->>" + index.toString());
                    return ListItemDetailsScreen(indexPositionData: index);
                  }));
                  // ItemListData.listTitles.insert(index, 'Planet');
                });
              },
              onLongPress: () {
                //                            <-- onLongPress
                setState(() {
                  //listTitles.removeAt(index);
                });
              },
            ),
          ),
          background: slideRightBackground(),
          secondaryBackground: slideLeftBackground(),
          confirmDismiss: (direction) async {
            if (direction == DismissDirection.endToStart) {
              final bool res = await showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      content: Text(
                          "Are you sure you want to delete ${ItemListData.listTitles[index]}?"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text(
                            "Cancel",
                            style: TextStyle(color: Colors.black),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        FlatButton(
                          child: Text(
                            "Delete",
                            style: TextStyle(color: Colors.red),
                          ),
                          onPressed: () {
                            // TODO: Delete the item from DB etc..
                            setState(() {
                              ItemListData.listTitles.removeAt(index);
                            });
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  });
              return res;
            } else {
              showEditDeleteAlert(
                  context, index, "Are you sure you want to edit this?");
              // TODO: Navigate to edit page;
            }
          },
        );
      },
    );
  }

  Future<String> _onShareData(BuildContext context) async {
    final RenderBox box = context.findRenderObject();
    {
      await Share.share("my share",
          subject: "Subject share data..",
          sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    }
  }

  showEditDeleteAlert(BuildContext context, int index, String message) {
    // set up the buttons
    Widget okButton = FlatButton(
      child: Text("Yes"),
      onPressed: () {
        //Navigator.of(context).pop();

/*        Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) {
                  print(cat);
                  return new Manu_screen(cat);
                }
            )
        );*/

        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) {
          print("Clicked Pass Data ---====-->>" + index.toString());
          return ListItemDetailsScreen(indexPositionData: index);
        }));

        //ItemListData.listTitles.removeAt(index);
        //Navigator.of(context).popUntil((route) => route.isFirst);
      },
    );
    Widget cancelButton = FlatButton(
      child: Text("No"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Delete/Edit Alert"),
      content: Text(message),
      actions: [
        okButton,
        cancelButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    //display image selected from gallery

    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text(
          "List Data",
        ),
        backgroundColor: Colors.green,
        actions: [
          IconButton(
            icon: const Icon(
              Icons.share,
              color: Colors.black,
            ),
            onPressed: () {
              // final RenderBox box = context.findRenderObject();
              _onShareData(context);
            },
          ),
        ],
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("pull up load");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("Load Failed!Click retry!");
            } else if (mode == LoadStatus.canLoading) {
              body = Text("release to load more");
            } else {
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: new Builder(
          builder: (BuildContext context) {
            return _myListView();
          },
        ),
      ),
    );
  }
}
