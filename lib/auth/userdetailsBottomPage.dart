import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';

import 'package:flutter/cupertino.dart';

import 'package:flutter_app/viewmodel/profileViewModel.dart';
import 'package:intl/intl.dart';

import '../model/response_login.dart';

import '../comman/app_themes.dart';
import '../comman/custom_button.dart';
import '../comman/size_config.dart';
import '../comman/intel_utility.dart';
import '../comman/pref_utils.dart';

import 'package:latlong/latlong.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';

class UserDetailsBottomPage extends StatefulWidget {
  @override
  _UserDetailsState createState() => _UserDetailsState();
}

enum ChooseData { Camera, Gallery }

class _UserDetailsState extends State<UserDetailsBottomPage> {
  final fullNameControler = TextEditingController();
  final emailControler = TextEditingController();
  final mobileControler = TextEditingController();

  final dobControler = TextEditingController();

  ProfileViewModel _viewModel = ProfileViewModel();

  String _selectedDropDwnData;

  List<String> alDropList = [
    'Red Color',
    'Blue Color',
    'Orange Color',
    'Dell Color'
  ];

  //Using Camera Regarding
/*  CameraController cameraControler;

  PermissionStatus _status;*/

  BuildContext mContext;

  @override
  void initState() {
    _getInitialData();
    super.initState();
    _getUserLocation();
  }

  LatLng currentPostion;

  void _getUserLocation() async {
    var position = await GeolocatorPlatform.instance
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    setState(() {
      currentPostion = LatLng(position.latitude, position.longitude);
      print("Get Current Location-----===> " + currentPostion.toString());
      gettingAddressName(currentPostion);
    });
  }

  gettingAddressName(LatLng gettingAddressName) async {
    final coordinates = new Coordinates(
        gettingAddressName.latitude, gettingAddressName.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var firstName = addresses.first;
    print(
        "${firstName.featureName} : ${firstName.addressLine}: ${firstName.adminArea}: ${firstName.countryCode}"
        ": ${firstName.countryName} : ${firstName.locality} : ${firstName.postalCode} : ${firstName.subLocality}");
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    super.dispose();
  }

  //Camera

  void _getInitialData() {
    setState(() {
      Preferences.getString(Preferences.userDetails).then((pref) {
        print("auth data----===-> " + pref);

        if (pref != null) {
          UserDetails userDetails = responseLogInUserFromJson(pref);

          fullname = userDetails.fullName;
          email = userDetails.email;
          pincode = userDetails.pincode;
          _dob = userDetails.dob;

          fullNameControler.text = fullname;
          emailControler.text = email;
          mobileControler.text = mobile;
          dobControler.text = _dob;
          print("Full Name:::::==--=> " + fullname);
          print("Email:::::==--=> " + email);
          print("pincode:::::==--=> " + pincode);
        }
      });
    });
  }

  void _displayOptionsDialog() async {
    await _optionsDialogBox();
  }

  Future<void> _optionsDialogBox() {
    return showDialog(
      context: mContext,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                GestureDetector(
                  child: Text('Take Photo'),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                GestureDetector(
                  child: Text('Take Gallery'),
                  //onTap: imageSelectGallery,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<File> imageFile;

  pickImageFromGallery(ImageSource source) {
    setState(() {
      imageFile = ImagePicker.pickImage(source: source);
    });
  }

  Widget showImage() {
    return FutureBuilder<File>(
      future: imageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return Image.file(
            snapshot.data,
            width: 300,
            height: 300,
          );
        } else if (snapshot.error != null) {
          return const Text(
            'Error Picking Image',
            textAlign: TextAlign.center,
          );
        } else {
          return const Text(
            'No Image Selected',
            textAlign: TextAlign.center,
          );
        }
      },
    );
  }

  File cameraFile;

  selectFromCamera() async {
    cameraFile = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      //maxHeight: 500.0,
      //maxWidth: 500.0,
    );
    setState(() {});
  }

  Widget _setRoundedProfileImage() {
    return InkWell(
      onTap: () {
        selectFromCamera();
      },
      //showImageChooseDialog(),
      child: Container(
        height: 40.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: SizeConfig.safeBlockVertical * 1,
            ),
            Container(
              //padding: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0, bottom: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                      radius: 40.0,
                      //backgroundImage: NetworkImage("http://tineye.com/images/widgets/mona.jpg"),
                      child: cameraFile == null
                          ? Center(
                              child: IconButton(
                                  //icon: Icon(Icons.offline_bolt),
                                  icon: Image.asset("assets/images/user.png")),
                            )
                          : Center(child: new Image.file(cameraFile))),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  //File _image;

  String fullname = "Full Name",
      email = "Email",
      pincode = "Pincode",
      mobile = "",
      _dob = "21-04-2021";

  Widget _inputFields(BuildContext context) {
    return Container(
      child: Form(
        child: Column(
          children: [
            TextField(
              decoration: InputDecoration(
                hintText: "Full Name",
                labelText: fullname,
              ),
              controller: fullNameControler,
              // onSubmitted: _updateButtons(),
            ),
            TextField(
              decoration: InputDecoration(
                hintText: "Email",
                labelText: email,
              ),
              controller: emailControler,
              keyboardType: TextInputType.emailAddress,
              // onSubmitted: _updateButtons(),
            ),
            TextField(
              decoration: InputDecoration(
                hintText: "Mobile Number",
                labelText: mobile,
              ),
              controller: mobileControler,
              keyboardType: TextInputType.number,
              // onSubmitted: _updateButtons(),
            ),
            changedDateOfBirth(context),
            SizedBox(
              height: SizeConfig.safeBlockVertical * 1,
            ),
            _dropDownList(context),
          ],
        ),
      ),
    );
  }

  Widget _dropDownList(BuildContext context) {
    return Container(
      child: DropdownButton(
        hint: Text('Please choose a location'), // Not necessary for Option 1
        value: _selectedDropDwnData,
        onChanged: (newValue) {
          setState(() {
            _selectedDropDwnData = newValue;
          });
        },
        items: alDropList.map((location) {
          return DropdownMenuItem(
            child: Text(
              location,
              style: TextStyle(
                color: Colors.red,
              ),
            ),
            value: location,
          );
        }).toList(),
      ),
    );
  }

  void _updateProfile(BuildContext context) {
    final name = fullNameControler.text;
    final email = emailControler.text;
    final mobile = mobileControler.text;

    if (name.isEmpty) {
      //showAlertDialog(context, "Please enter full name.");
      IntelUtility.showAlertToast(context, "Please enter full name.");
      return;
    } else if (email.isEmpty) {
      //showAlertDialog(context, "Please enter email id.");
      IntelUtility.showAlertToast(context, "Please enter email id.");
      return;
    } else if (!IntelUtility.isValidEmail(email)) {
      //showAlertDialog(context, "Please enter valid email id.");
      IntelUtility.showAlertToast(context, "Please enter valid email id.");
      return;
    } else if (mobile.isEmpty) {
      //showAlertDialog(context, "Please enter mobile number");
      IntelUtility.showAlertToast(context, "Please enter mobile number");
      return;
    } else if (mobile.length < 10) {
      IntelUtility.showAlertToast(context, "Please enter valid mobile number.");
      //showAlertDialog(context, "Please enter valid mobile number.");
      return;
    } else {
      callUpdateApicall(context);
    }
  }

  void callUpdateApicall(BuildContext context) {
    _viewModel.fullName = fullNameControler.text.toString();
    _viewModel.email = emailControler.text.toString();
    //_viewModel.mobile=mobileControler.text.toString();
    _viewModel.dob = IntelUtility.getConvertedDate(_dob);

    IntelUtility.showLoader(true, context);
    _viewModel.performUpdateProfile(
      (success, message) {
        print("success-----==-->" + success.toString());
        print("message-----==-->" + message);

        IntelUtility.showLoader(false, context);
        if (success) {
          IntelUtility.showSuccessToast(
              context, "Update Profile Successfully.");
          Navigator.of(context).pop();
          //userProfileState.refreshProfile();
        } else {
          IntelUtility.showFailedToast(context, message);
        }
      },
    );
  }

  showAlertDialog(BuildContext context, String message) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Ok"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text(message),
      actions: [
        cancelButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget updateProfileButton(BuildContext context) {
    return InkWell(
      onTap: () => _updateProfile(context),
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 5),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.brown,
              /* offset: Offset(2, 4),
              blurRadius: 5,
              spreadRadius: 2,*/
            ),
          ],
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [Color(0xfffbb448), Color(0xfffbb448)],
          ),
        ),
        child: filledButton(
          text: 'Update Profile',
          fontSize: SizeConfig.safeBlockVertical * 2,
          radius: 5.0,
          textColor: AppThemes.whiteColor,
        ),
      ),
    );
  }

  Widget changedDateOfBirth(BuildContext context) {
    return GestureDetector(
      onTap: () => openDatePicker(context),
      child: AbsorbPointer(
        child: TextField(
          decoration: InputDecoration(
            hintText: "Date of birth",
            labelText: _dob,
          ),
          onSubmitted: (value) => _dob = value,
          controller: dobControler,
          keyboardType: TextInputType.datetime,
          // onSubmitted: _updateButtons(),
        ) /* CustomTextField(
          obscure: false,
          maxLine: 1,
          color: AppThemes.lightGrayColor,
          hint: "01-01-1900",
          labelText: "Date of birth",
          controller: dobControler,
          onSaved: (value) => _dob = value,
          keyBoardType: TextInputType.datetime,
          prefixText: CustomIcons.cake,
          enable: false,
        )*/
        ,
      ),
    );
  }

  void openDatePicker(BuildContext context) {
    showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1960),
        lastDate: DateTime.now(),
        builder: (context, child) {
          return Theme(
            data: ThemeData.light(),
            child: child,
          );
        }).then((picked) {
      if (picked != null) {
        DateFormat displayDate = DateFormat("dd-MM-yyyy");
        setState(() {
          dobControler.text = displayDate.format(picked);
          _dob = dobControler.text;
        });
      }
    });
  }

  File _image;

  Future getImagefromCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    mContext = context;

    return GestureDetector(
      child: Scaffold(
        backgroundColor: AppThemes.whiteColor,
        body: SingleChildScrollView(
          child: Column(
            children: [
              _setRoundedProfileImage(),
              SizedBox(
                height: SizeConfig.safeBlockVertical * 10,
              ),
              Container(
                margin: EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    _inputFields(context),
                    SizedBox(
                      height: SizeConfig.safeBlockVertical * 5,
                    ),
                    updateProfileButton(context),
                    SizedBox(
                      height: SizeConfig.safeBlockVertical * 5,
                    ),
                    FlatButton(
                      color: Colors.grey,
                      onPressed: () => _updateProfile(context),
                      child: Text(
                        "Flat button Update ",
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.white,
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.safeBlockVertical * 10,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );

    /*Container(
      color: Colors.grey,
      child: Center(
          child: Text(
        "User Profile Screen",
        style: TextStyle(fontSize: 20, color: Colors.white),
      )),
    );*/
  }
}
