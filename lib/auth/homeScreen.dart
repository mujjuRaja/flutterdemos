import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/auth/tabDataScreen.dart';

import '../auth/messageBottomPage.dart';
import '../auth/homeBottomPage.dart';
import '../auth/userdetailsBottomPage.dart';
import '../auth/GoogleMapScreen.dart';
import '../auth/swipeListScreen.dart';
import '../auth/firebaseDatabaseScreen.dart';
import '../auth/loginScreen.dart';
import '../auth/horizontalViewPage.dart';
import '../auth/tabview/tabTextDataScreen.dart';
import '../auth/fcm/firebaseRealTimeScreen.dart';

import '../comman/pref_utils.dart';
import '../comman/intel_utility.dart';
import '../comman/permissionService.dart';

import '../model/transaction.dart';
import '../widgets/placeholderWidget.dart';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'package:fluttertoast/fluttertoast.dart';

class HomeScreen extends StatefulWidget {
  static const String tag = "/home_view";

  HomeScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _counter = 0;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();

  int _currentIndex = 0;
  final List<Widget> _children = [
    PlaceholderWidget(Colors.white),
    PlaceholderWidget(Colors.deepOrange),
    PlaceholderWidget(Colors.green)
  ];

  /* void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }*/

  final List<Transaction> _userTransaction = [
    Transaction(
      id: "t1",
      title: "New Shoes",
      amount: 98.58,
      date: DateTime.now(),
    ),
    Transaction(
      id: "t2",
      title: "New Hair cut",
      amount: 20.00,
      date: DateTime.now(),
    ),
    Transaction(
      id: "t3",
      title: "New Box",
      amount: 58.58,
      date: DateTime.now(),
    ),
  ];

  void _addNewTransaction(String txTitle, double txAmount) {
    final newTx = Transaction(
      title: txTitle,
      amount: txAmount,
      date: DateTime.now(),
      id: DateTime.now().toString(),
    );

    setState(() {
      _userTransaction.add(newTx);
    });
  }

  showLogoutAlertDialog(BuildContext context, String message) {
    // set up the buttons
    Widget okButton = FlatButton(
      child: Text("Ok"),
      onPressed: () {
        Navigator.of(context).popUntil((route) => route.isFirst);
        IntelUtility.navigateReplaceToScreen(context, LoginScreen());
        Preferences.clearData(Preferences.isLogin);
      },
    );
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Logout"),
      content: Text(message),
      actions: [
        okButton,
        cancelButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showNavigationBar() {}

  final List<Transaction> transaction = [];
  final titleController = TextEditingController();
  final amountController = TextEditingController();

  int index = 0;

  int _pageIndex = 0;
  String _tabTitle = "";
  PageController _pageController;

  List<Widget> slideMenuPages = [
    HomeBottomPage(),
    MessageBottomPage(),
    UserDetailsBottomPage(),
    HorizontalViewPage(),
    GoogleMapScreen(),
    TabDataScreen(),
    TabTextDataScreen(),
    SwipeListScreen(),
    FirebaseDatabaseScreen(),
    FirebaseRealTimeScreen()
  ];

  List<String> slideMenuTabName = [
    "List View",
    "Grid View",
    "Profile",
    "Horizontal View",
    "Google Map",
    "Tab Image Data",
    "Tab Text Data",
    "Swipe List",
    "Firebase Realtime",
    "FCM Realtime"
  ];


  @override
  void initState() {
    permissionAcessPhone();
    regardingPushNotification();
    super.initState();
    _pageController = PageController(initialPage: _pageIndex);
    _tabTitle = 'Home';
  }

//Regarding Push notificaiton.............Start

  regardingPushNotification() {
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/ic_launcher');

    var initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidRecieveLocalNotification);

    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('on message :------=-----====>> ${message}');
        // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
        displayNotification(message);
        // _showItemDialog(message);
      },
      onResume: (Map<String, dynamic> message) {
        print('on Resume :------=-----====>> ${message}');
      },
      onLaunch: (Map<String, dynamic> message) {
        print('on Launch :------=-----====>> ${message}');
      },
    );

    //TOdo: FCM regarding permission set: alert/badge/sound
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, alert: true, badge: true));

    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });

    //TOdo: FCM regarding get Firebase initialized token
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      print("firebase token:=-----====>>" + token);
    });
  }

  Future onDidRecieveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () async {
              //Navigator.of(context, rootNavigator: true).pop();
              IntelUtility.navigatePushToScreen(
                  context, MessageBottomPage());
              await Fluttertoast.showToast(
                  toastLength: Toast.LENGTH_SHORT,
                  msg: "Notification Open",
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black54,
                  textColor: Colors.red,
                  fontSize: 16.0);
            },
          )
        ],
      ),
    );
  }

  Future displayNotification(Map<String, dynamic> message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      'channelid',
      'flutterfcm',
      'your channel description',
      importance: Importance.Max,
      priority: Priority.High,
      playSound: true,
    );

    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      message['notification']['title'],
      message['notification']['body'],
      platformChannelSpecifics,
      payload: 'hello',
    );
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }

    IntelUtility.navigatePushToScreen(context, HorizontalViewPage());

    await Fluttertoast.showToast(
        msg: "Notification Clicked",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
        fontSize: 16.0);
    /*Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => new SecondScreen(payload)),
    );*/
  }

  //Regarding Push notificatoin End...............

  Future permissionAcessPhone() {
    PermissionService().requestPermission(onPermissionDenied: () {
      print('Permission has been denied');
    });
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(_tabTitle), actions: <Widget>[
        IconButton(
          //icon: Icon(Icons.offline_bolt),
          icon: Image.asset("assets/images/ic_logout.png"),
          onPressed: () => {
            showLogoutAlertDialog(context, "Are you sure you want to logout?")
          },
        ),
      ]),
      drawer: Drawer(
        elevation: 5,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountEmail: Text(''),
              accountName: Row(
                children: <Widget>[
                  Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(shape: BoxShape.circle),
                    //Todo: Assets image set in drawer layout
                    child: CircleAvatar(
                      radius: 50.0,
                      backgroundColor: const Color(0xFF778899),
                      backgroundImage: NetworkImage(
                          "http://tineye.com/images/widgets/mona.jpg"),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text('mujju'),
                      Text('mujju@gmail.com'),
                    ],
                  ),
                ],
              ),
            ),
            ListTile(
              title: Text('Home'),
              onTap: () {
                onSlideTabTapped(0);
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Message'),
              onTap: () {
                onSlideTabTapped(1);
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Profile'),
              onTap: () {
                onSlideTabTapped(2);
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Horizontal View'),
              onTap: () {
                onSlideTabTapped(3);
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Google Map View'),
              onTap: () {
                onSlideTabTapped(4);
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Tab Image View'),
              onTap: () {
                onSlideTabTapped(5);
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Tab Text View'),
              onTap: () {
                onSlideTabTapped(6);
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Swipe List View'),
              onTap: () {
                onSlideTabTapped(7);
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Firebase Real time DB'),
              onTap: () {
                onSlideTabTapped(8);
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Firebase DB'),
              onTap: () {
                onSlideTabTapped(9);
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: PageView(
        children: slideMenuPages,
        onPageChanged: onPageChanged,
        controller: _pageController,
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add,color: Colors.white, size: 29),
        backgroundColor: Colors.greenAccent,
        tooltip: 'Capture Picture',
        elevation: 5,
        splashColor: Colors.grey,
        onPressed: (){
          openBottomDialogView();
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,

      /* bottomNavigationBar: BottomNavigationBar(
        currentIndex: _pageIndex,
        onTap: onBottomTabTapped,
        backgroundColor: Colors.white,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("Home"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.mail),
            title: Text("Messages"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text("Profile"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.supervised_user_circle_sharp),
            title: Text("Profile"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.map),
            title: Text("Google Map"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.tab),
            title: Text("Tab data"),
          ),
        ],
      ),*/
    );
  }



  openBottomDialogView(){
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: new Icon(Icons.photo),
                trailing: Icon(Icons.card_travel),
                title: new Text('Photo'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: new Icon(Icons.music_note),
                title: new Text('Music'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: new Icon(Icons.videocam),
                title: new Text('Video'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: new Icon(Icons.share),
                title: new Text('Share'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  void onPageChanged(int page) {
    setState(() {
      this._pageIndex = page;
    });
  }

  /* void onBottomTabTapped(int index) {
    print("tab index----==>" + index.toString());
    this._pageController.animateToPage(index,
        duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
    this._tabTitle = slideMenuTabName[index];
  }
*/
  void onSlideTabTapped(int index) {
    print("tab index----==>" + index.toString());
    this._pageController.animateToPage(index, duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
    this._tabTitle = slideMenuTabName[index];
  }
}
