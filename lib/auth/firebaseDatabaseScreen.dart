import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FirebaseDatabaseScreen extends StatefulWidget {
  @override
  _FirebaseDatabaseScreenState createState() => _FirebaseDatabaseScreenState();
}

class _FirebaseDatabaseScreenState extends State<FirebaseDatabaseScreen> {
  final fb = FirebaseDatabase.instance;
  final mController = TextEditingController();
  final message = "Message";
  final name = "username";

  @override
  Widget build(BuildContext context) {
    final ref = fb.reference();
    var retrievedName;

    return Scaffold(
      appBar: AppBar(
        title: Text('Firebase Real time database'),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(name),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: TextField(
                    controller: mController,
                  ),
                )
              ],
            ),
            ElevatedButton(
              onPressed: () {
                ref.child(message).child(name).set(mController.text);
              },
              child: Text('Submit'),
            ),

            ElevatedButton(
              onPressed: () {
                ref..child(message).child("Name").once().then((DataSnapshot data){
                  print("========---------"+data.value);
                  print("========---------"+data.key);
                  setState(() {
                    retrievedName = data.value;
                  });
                });
              },
              child: Text("Get"),
            ),
            Text(retrievedName ?? ""),
          ],
        ),
      ),
    );
  }


  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    mController.dispose();
    super.dispose();
  }
}
