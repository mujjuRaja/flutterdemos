import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:intl/intl.dart';

import '../auth/profile/user_profile.dart';
import '../viewmodel/profileViewModel.dart';

import '../comman/intel_utility.dart';
import '../comman/validation_utils.dart';
import '../comman/app_themes.dart';
import '../comman/commonClass.dart';
import '../comman/custom_button.dart';
import '../comman/custom_icons.dart';
import '../comman/custom_text_field.dart';
import '../comman/size_config.dart';

class ProfileBottomPage extends StatefulWidget {
  @override
  _ProfileBottomPageState createState() => _ProfileBottomPageState();
}

class _ProfileBottomPageState extends State<ProfileBottomPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _fullName = "", _email = "", _dob = "";

  var _nameController = TextEditingController();
  var _emailController = TextEditingController();
  var _dobController = TextEditingController();
  ProfileViewModel _viewModel = ProfileViewModel();

  List images = [
    Image.asset(CustomIcons.forestIcon),
    Image.asset(CustomIcons.forestIcon),
    Image.asset(CustomIcons.forestIcon),
    Image.asset(CustomIcons.forestIcon),
  ];

  @override
  void initState() {
    _getInitialData();
    super.initState();
  }

  _getInitialData() {
    setState(() {
      _fullName = CommonClass.authUser.fullName;
      _email = CommonClass.authUser.email;
      _dob = CommonClass.authUser.dob;

      _nameController.text = _fullName;
      _emailController.text = _email;
      _dobController.text = _dob;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CommonClass.profileAppBar(context),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _carousel(),
              SizedBox(height: 3),
              _inputFields(),
              Container(height: MediaQuery.of(context).size.height * 0.1),
              _saveButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _carousel() {
    return Center(
      child: Container(
        height: 220,
        width: 150,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Carousel(
            images: images,
            boxFit: BoxFit.none,
            dotSize: 5,
            dotBgColor: Colors.transparent,
            dotSpacing: 10,
            dotPosition: DotPosition.bottomCenter,
            dotColor: AppThemes.arrowColor,
            indicatorBgPadding: 8,
            dotIncreasedColor: AppThemes.mainAppColor,
            animationCurve: Curves.fastOutSlowIn,
            animationDuration: Duration(seconds: 1),
          ),
        ),
      ),
    );
  }

  Widget _inputFields() {
    return Container(
      child: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: Column(
          children: [
            CustomTextField(
              obscure: false,
              maxLine: 1,
              color: AppThemes.lightGrayColor,
              hint: "John Smith",
              labelText: "Username",
              controller: _nameController,
              onSaved: (value) => _fullName = value,
              validator: (value) => value.isEmpty ? "Enter Full Name" : null,
              keyBoardType: TextInputType.emailAddress,
              prefixWidget: CustomIcons.user,
            ),
            CustomTextField(
              obscure: false,
              maxLine: 1,
              color: AppThemes.lightGrayColor,
              hint: "johnsmith@gmail.com",
              labelText: "Email",
              controller: _emailController,
              onSaved: (value) => _email = value,
              validator: Validations.validateEmail,
              keyBoardType: TextInputType.emailAddress,
              prefixWidget: CustomIcons.envelope,
            ),
            GestureDetector(
              onTap: () {
                datePicker();
              },
              child: AbsorbPointer(
                child: CustomTextField(
                  obscure: false,
                  maxLine: 1,
                  color: AppThemes.lightGrayColor,
                  hint: "01-01-1990",
                  labelText: "Date",
                  controller: _dobController,
                  onSaved: (value) => _dob = value,
                  keyBoardType: TextInputType.datetime,
                  prefixWidget: CustomIcons.cake,
                  enable: false,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void datePicker() {
    showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1960),
        lastDate: DateTime.now(),
        builder: (context, child) {
          return Theme(
            data: ThemeData.light(),
            child: child,
          );
        }).then((picked) {
      if (picked != null) {
        DateFormat displayformat = DateFormat("dd-MM-yyyy");
        setState(() {
          _dobController.text = displayformat.format(picked);
          _dob = _dobController.text;
        });
      }
    });
  }

  Widget _saveButton() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: filledButton(
        text: "SAVE",
        fontSize: SizeConfig.safeBlockHorizontal * 4,
        radius: 5.0,
        textColor: AppThemes.whiteColor,
        function: _validateInput,
      ),
    );
  }

  _validateInput() {
    final FormState form = _formKey.currentState;
    if (_formKey.currentState.validate()) {
      form.save();
      callUpdateApicall();
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }

  void callUpdateApicall() {
    _viewModel.fullName = _fullName;
    _viewModel.email = _email;
    _viewModel.dob = IntelUtility.getConvertedDate(_dob);

    IntelUtility.showLoader(true, context);
    _viewModel.performUpdateProfile((success, message) {
      IntelUtility.showLoader(false, context);
      IntelUtility.showNotificationAlert(message);
      if (success) {
        userProfileState.refreshProfile();
        Navigator.pop(context);
      }
    });
  }
}
