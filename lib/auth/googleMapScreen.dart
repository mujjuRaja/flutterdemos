import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_app/comman/intel_utility.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';

import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoogleMapScreen extends StatefulWidget {
  @override
  _GoogleMapScreenState createState() => _GoogleMapScreenState();
}

class _GoogleMapScreenState extends State<GoogleMapScreen> {
  Completer<GoogleMapController> _controller = Completer();

  LatLng _latLngCurrentLocation = LatLng(23.125837, 72.057442);
  LatLng _CurrentLocation = LatLng(23.125837, 72.057442);
  String currentAddress;

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  @override
  void initState() {
    super.initState();
    _getUserLocation();
  }

//Moved the camera as per finger fill
  void _onCameraMove(CameraPosition position) {
    _latLngCurrentLocation = position.target;
  }

  //var googleMap = GoogleMap();

  void _getUserLocation() async {
    var position = await GeolocatorPlatform.instance
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    setState(() {
      _latLngCurrentLocation = LatLng(position.latitude, position.longitude);

      gettingAddressName(_latLngCurrentLocation);

      print(
          "Get Current Location-----===> " + _latLngCurrentLocation.toString());
    });
  }

  gettingAddressName(LatLng gettingAddressName) async {
    final coordinates = new Coordinates(
        gettingAddressName.latitude, gettingAddressName.longitude);
    var addresses =
    await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var firstName = addresses.first;
    print(
        "${firstName.featureName} : ${firstName.addressLine}: ${firstName
            .adminArea}: ${firstName.countryCode}"
            ": ${firstName.countryName} : ${firstName.locality} : ${firstName
            .postalCode} : ${firstName.subLocality}");

    setState(() {
      currentAddress = firstName.addressLine;

      _markers.add(
        Marker(
            markerId: MarkerId(_latLngCurrentLocation.toString()),
            position: _latLngCurrentLocation,
            infoWindow: InfoWindow(
              title: 'Current Location',
              snippet: currentAddress,
            ),
            onTap: () {
              IntelUtility.showAlertToast(context, currentAddress);
              print("After clicked-------====-->" + _markers.first.toString());
            },
            icon: BitmapDescriptor.defaultMarker),
      );
    });
  }

  final Set<Marker> _markers = {};

  final Set<Polyline>_polyline = {};

//add your lat and lng where you wants to draw polyline
  List<LatLng> _latlng = List();
  LatLng _new = LatLng(33.738045, 73.084488);
  LatLng _news = LatLng(33.567997728, 72.635997456);

  Map<PolylineId, Polyline> _mapPolylines = {};
  int _polylineIdCounter = 1;

  List<LatLng> _createPoints() {
    final List<LatLng> points = <LatLng>[];
    points.add(LatLng(23.125837, 72.057442));
    points.add(LatLng(23.105837, 72.047442));
    points.add(LatLng(23.126837, 72.055442));
    points.add(LatLng(23.126837, 72.057542));
    return points;
  }

  void _add() {
    final String polylineIdVal = 'polyline_id_$_polylineIdCounter';
    _polylineIdCounter++;
    final PolylineId polylineId = PolylineId(polylineIdVal);

    final Polyline polyline = Polyline(
      polylineId: polylineId,
      consumeTapEvents: true,
      color: Colors.red,
      width: 5,
      points: _createPoints(),
    );

    setState(() {
      _mapPolylines[polylineId] = polyline;
    });


    _polyline.add(Polyline(
      polylineId: PolylineId(_latLngCurrentLocation.toString()),
      visible: true,
      //latlng is List<LatLng>
      points: _latlng,
      color: Colors.blue,
    ));
  }

  @override
  Widget build(BuildContext context) {

    try {
      print('_latLngCurrentLocation ---===---=---> ' +
          _latLngCurrentLocation.toString());
      print('_CurrentLocation ---===---=---> ' + _CurrentLocation.toString());

      _latlng.add(_new);
      _latlng.add(_news);
    }catch(error){

    }

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Maps Sample App'),
          actions: <Widget>[IconButton(icon: Icon(Icons.add), onPressed: _add)],
          backgroundColor: Colors.green[500],
        ),
        body: GoogleMap(
          polylines:_polyline,
          markers: _markers,
          onMapCreated: _onMapCreated,
          onCameraMove: _onCameraMove,
          zoomControlsEnabled: true,
          zoomGesturesEnabled: true,
          scrollGesturesEnabled: true,
          compassEnabled: true,
          rotateGesturesEnabled: true,
          mapToolbarEnabled: true,
          tiltGesturesEnabled: true,
          gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
            new Factory<OneSequenceGestureRecognizer>(
                  () => new EagerGestureRecognizer(),
            ),
          ].toSet(),
          initialCameraPosition: CameraPosition(
            target: _latLngCurrentLocation ?? _CurrentLocation,
            zoom: 11.0,
          ),
          //  polylines: Set<Polyline>.of(_mapPolylines.values),
        ),
      ),
    );
  }
}
