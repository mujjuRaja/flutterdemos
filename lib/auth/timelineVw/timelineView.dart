import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/auth/timelineVw/timelineComponent.dart';
import 'package:flutter_app/model/timelineModel.dart';

class TimelineView extends StatelessWidget {
  List<TimelineModel> list = [
    TimelineModel(
        id: "1",
        description: "Send Order",
        title: "This is ."),
    TimelineModel(
        id: "2",
        description: "Check Order",
        title: "This is ."),
    TimelineModel(
        id: "3",
        description: "Accepted Order",
        title: "This is ."),
    TimelineModel(
        id: "4",
        description: "Preparder Order",
        title: "This is ."),
    TimelineModel(
        id: "5",
        description: "Packed Order",
        title: "This is ."),
    TimelineModel(
        id: "6",
        description: "On Going Order",
        title: "This is ."),
    TimelineModel(
        id: "7",
        description: "Reached Order",
        title: "This is ."),
    TimelineModel(
        id: "8",
        description: "Completed Order",
        title: "This is ."),
  ];

  @override
  Widget build(BuildContext context) {
    return new TimelineComponent(
      timelineList: list,
      lineColor: Colors.red[200],
      // Defaults to accent color if not provided
      backgroundColor: Colors.black26,
      // Defaults to white if not provided
      headingColor: Colors.red,
      // Defaults to black if not provided
      descriptionColor: Colors.grey, // Defaults to grey if not provided
    );
  }
}
