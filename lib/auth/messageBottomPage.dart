import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/comman/intel_utility.dart';
import 'package:flutter_app/model/itemListData.dart';

import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:geocoder/geocoder.dart';

import 'package:flutter_app/auth/sam/videoViewScreen.dart';


const kGoogleApiKey = "AIzaSyCcyTdIli2NRB3VHcKm5ws8eriCCgD9idw";
GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

class MessageBottomPage extends StatelessWidget {
  Future<Null> displayPrediction(Prediction p) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      var placeId = p.placeId;
      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;

      var address = await Geocoder.local.findAddressesFromQuery(p.description);
      print("------------->" + address.toString());
      print("------------->" + lat.toString());
      print("------------->" + lng.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Grid view List'),
          centerTitle: true,
          backgroundColor: Colors.greenAccent,
          actions: <Widget>[
            Row(
              children: [
                IconButton(
                  //icon: Icon(Icons.offline_bolt),
                  icon: Image.asset("assets/images/ic_home.png"),
                  onPressed: () => {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                              title: Text('Country List'),
                              content: showCountryListDialog());
                        }),
                  },
                ),
                IconButton(
                  //icon: Icon(Icons.offline_bolt),
                  icon: Image.asset("assets/images/ic_filter_blck.png"),
                  onPressed: () => {
                    showFancyCustomDialog(context),
                  },
                ),
                IconButton(
                  //icon: Icon(Icons.offline_bolt),
                  icon: Icon(Icons.ondemand_video,color: Colors.black,),
                  onPressed: () => {
                    IntelUtility.navigatePushToScreen(context, VideoViewScreen())
                  },
                ),
              ],
            ),
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(2),
          child: Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GridView.builder(
                itemCount: ItemListData.listIcons.length,

//Todo: Column-Row under used list OR Grid view so it's required add this "shrinkWrap", othor wise data not show in list
                shrinkWrap: true,

                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 1.0,
                  mainAxisSpacing: 1.0,
                ),
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    child: Column(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(ItemListData.listIcons[index]),
                          onPressed: () {
                            showConfirmAlert(context, index,
                                "Are you sure you want to Pass data?");
                          },
                        ),
                        Text(
                          ItemListData.listTitles[index],
                          maxLines: 2, //Singline show
                        ),
                      ],
                    ),
                  );
                },
              ),
              SizedBox(
                height: 5,
              ),
              RaisedButton(
                color: Colors.black,
                child: Text(
                  'Auto Search',
                  style: TextStyle(
                    color: Colors.green,
                    fontSize: 14,
                  ),
                ),
                onPressed: () async {
                  // show input autocomplete with selected mode
                  // then get the Prediction selected
                  Prediction p = await PlacesAutocomplete.show(
                      context: context, apiKey: kGoogleApiKey);
                  displayPrediction(p);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  //Custome Dialog
  Widget showCountryListDialog() {
    return Container(
      height: 300,
      width: double.maxFinite,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: 5,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(ItemListData.listTitles[index]),
            onTap: () {
              print("CLicked dialog data-----====--->" +
                  ItemListData.listTitles[index]);
              Navigator.of(context).pop();
            },
          );
        },
      ),
    );
  }

  void showFancyCustomDialog(BuildContext context) {
    Dialog fancyDialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0), color: Colors.purple),
        height: 500.0,
        width: double.maxFinite,
        child: Stack(
          children: <Widget>[
            Container(
              height: 300,
              width: double.maxFinite,
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: 5,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text(ItemListData.listTitles[index]),
                    onTap: () {
                      print("CLicked dialog data-----====--->" +
                          ItemListData.listTitles[index]);
                      Navigator.of(context).pop();
                    },
                  );
                },
              ),
            ),
            Container(
              width: double.infinity,
              height: 50,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(12),
                  topRight: Radius.circular(12),
                ),
              ),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  "Dialog Title!",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: double.infinity,
                  height: 50,
                  decoration: BoxDecoration(
                    color: Colors.blue[300],
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(12),
                      bottomRight: Radius.circular(12),
                    ),
                  ),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Okay let's go!",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ),
            ),
            Align(
              // These values are based on trial & error method
              alignment: Alignment(1.05, -1.05),
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Icon(
                    Icons.close,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
    showDialog(
        context: context, builder: (BuildContext context) => fancyDialog);
  }

  //Alert Dialog
  showConfirmAlert(BuildContext context, int index, String message) {
    // set up the buttons
    Widget okButton = FlatButton(
      child: Text("Yes"),
      onPressed: () {
        Navigator.of(context).pop();
        /* Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context){
              print("Clicked Pass Data ---====-->>"+index.toString());
              return ListItemDetailsScreen(indexPositionData: index);
            })
        );*/

        //ItemListData.listTitles.removeAt(index);
        //Navigator.of(context).popUntil((route) => route.isFirst);
      },
    );
    Widget cancelButton = FlatButton(
      child: Text("No"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("Pass Data Alert"),
      content: Text(message),
      actions: [
        okButton,
        cancelButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
