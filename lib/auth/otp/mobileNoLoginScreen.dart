import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/auth/otp/oTPVerifyScreen.dart';
import 'package:flutter_app/comman/intel_utility.dart';
import 'package:flutter_app/comman/size_config.dart';
import 'package:flutter_app/comman/custom_text_field.dart';
import 'package:flutter_app/comman/custom_button.dart';
import 'package:flutter_app/comman/validation_utils.dart';


class MobileNoLoginScreen extends StatefulWidget {

  @override
  _MobileNoLoginScreenState createState() => _MobileNoLoginScreenState();
}

class _MobileNoLoginScreenState extends State<MobileNoLoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  final TextEditingController _mobileNoControler = TextEditingController();
  String mobileNo;
  bool _autoValidate = false;

  Widget _inputFiledView() {
    return Container(
      padding: EdgeInsets.only(top: 15, right: 25, left: 25),
      child: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: Column(
          children: <Widget>[
            CustomTextField(
              labelText: 'Mobile no',
              hint: "Enter mobile no",
              color: Colors.black,
              keyBoardType: TextInputType.number,
              controller: _mobileNoControler,
              validator: Validations.validateMobile,
              onSaved: (value) => mobileNo = value,
            )
          ],
        ),
      ),
    );
  }

  Widget _getOtpButtosView() {
    return InkWell(
      onTap: () {
        IntelUtility.navigatePushToScreen(context, OTPVerifyScreen(mobileNo:mobileNo));
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(right: 25.0, left: 25.0),
        padding: EdgeInsets.symmetric(vertical: 5),
        alignment: Alignment.center,
        child: filledButton(
            text: "Get OTP",
            textColor: Colors.deepPurple,
            fontSize: SizeConfig.safeBlockHorizontal * 4,
            radius: 5.0,
        //    function: _validateInputeData
        ),
      ),
    );
  }

    _validateInputeData() {
      final FormState form = _formKey.currentState;
      if (_formKey.currentState.validate()) {
        form.save();
        //IntelUtility.navigatePushToScreen(context, OTPVerifyScreen());
      } else {
        setState(() {
          _autoValidate = true;
        });
      }
    }

  double _height, _width, _fixedPadding;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  final scaffoldKey =
      GlobalKey<ScaffoldState>(debugLabel: "scaffold-get-phone");

  @override
  Widget build(BuildContext context) {
    //  Fetching height & width parameters from the MediaQuery
    //  _logoPadding will be a constant, scaling it according to device's size
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _fixedPadding = _height * 0.025;

    SizeConfig().init(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_backspace,
            size: 25,
          ),
          onPressed: () {
            Navigator.of(context).pop;
          },
        ),
      ),
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _inputFiledView(),
            SizedBox(
              height: SizeConfig.safeBlockVertical * 1,
            ),
            _getOtpButtosView(),
            SizedBox(
              height: SizeConfig.safeBlockVertical * 4,
            ),
          ],
        ),
      ),
    );
  }
}
