import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../comman/intel_utility.dart';
import '../auth/tabview/tabTextDataScreen.dart';

class TabDataScreen extends StatefulWidget {
  @override
  _TabDataScreenState createState() => _TabDataScreenState();
}

class _TabDataScreenState extends State<TabDataScreen>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectIndex = 0;

  List<Widget> tabImageList = [
    Tab(icon: Icon(Icons.card_travel)),
    Tab(icon: Icon(Icons.add_shopping_cart)),
    Tab(icon: Icon(Icons.add)),
  ];

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: tabImageList.length, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectIndex = _controller.index;
      });
      print("Select Index :: " + _controller.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Tab Demo'),
          bottom: TabBar(
            //isScrollable: true,
            indicator: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: Colors.greenAccent,
            ),
            // indicatorColor: Colors.red,
            onTap: (index) {
              print("clicked tab position----====-->");
            },
            controller: _controller,
            tabs: tabImageList,
          ),
        ),
        body: TabBarView(
          controller: _controller,
          children: tabImageList.map((Widget tab) {
            return new Center(
                child: Text(
              tab.toString(),
              style: TextStyle(
                fontSize: 30,
                fontStyle: FontStyle.italic,
              ),
            ));
          }).toList(),
          //Todo:Single View Create as per clicked tab related
          /*children: [
            Center(
              child: Text(
                _selectIndex.toString(),
                style: TextStyle(fontSize: 40),
              ),
            ),
            Center(
              child: Text(
                _selectIndex.toString(),
                style: TextStyle(fontSize: 40),
              ),
            ),
            Center(
              child: Text(
                _selectIndex.toString(),
                style: TextStyle(fontSize: 40),
              ),
            ),
          ],*/
        ),
      ),
    );
  }
}
