import 'dart:async';
import 'dart:ui';

import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/comman/app_themes.dart';
import 'package:flutter_app/comman/commonClass.dart';
import 'package:flutter_app/comman/custom_button.dart';
import 'package:flutter_app/comman/custom_icons.dart';
import 'package:flutter_app/comman/size_config.dart';
import 'package:flutter_app/auth/profileBottomPage.dart';

UserProfileState userProfileState;

class UserProfile extends StatefulWidget {
  @override
  UserProfileState createState() {
    userProfileState = UserProfileState();
    return userProfileState;
  }
}

class UserProfileState extends State<UserProfile> {
  List<Image> imagelist = List();

  @override
  void initState() {
    super.initState();
    imagelist.add(Image.asset(CustomIcons.avfcIcon));
    imagelist.add(Image.asset(CustomIcons.forestIcon));
  }

  refreshProfile() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      height: 220,
                      child: Carousel(
                        boxFit: BoxFit.cover,
                        images: imagelist,
                        indicatorBgPadding: 8.0,
                        dotBgColor: Colors.transparent,
                        autoplay: false,
                        dotSpacing: 10.0,
                        animationCurve: Curves.fastOutSlowIn,
                        animationDuration: Duration(seconds: 1),
                        dotIncreasedColor: AppThemes.dotColor(),
                        dotIncreaseSize: 5.0,
                        dotColor: AppThemes.dotnormalColor(),
                      ),
                    ),
                    flex: 1,
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            CommonClass.authUser.fullName == null
                                ? ""
                                : CommonClass.authUser.fullName,
                            style: AppThemes.textStyle(3.5).copyWith(
                                fontWeight: FontWeight.w700,
                                color: Colors.black),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            CommonClass.authUser.email == null
                                ? ""
                                : CommonClass.authUser.email,
                            style: AppThemes.textStyle(3.5)
                                .copyWith(color: Colors.black),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          CommonClass.authUser.dob != null &&
                                  CommonClass.authUser.dob.length > 0
                              ? Text(
                                  CommonClass.authUser == null
                                      ? ""
                                      : CommonClass.authUser.dob,
                                  style: AppThemes.textStyle(3.5)
                                      .copyWith(color: Colors.black),
                                )
                              : Container(),
                          SizedBox(
                            height: 10,
                          ),
                          _EditButton()
                        ],
                      ),
                    ),
                    flex: 2,
                  )
                ],
              ),
              Container(
                width: 220,
                decoration: BoxDecoration(
                    border: Border.all(
                      color: AppThemes.appMainThemeColor(),
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(15))),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 5, bottom: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Change sport",
                        style: AppThemes.textStyleNormalWith(
                          fontSize: 16,
                          color: AppThemes.appMainThemeColor(),
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Container(
                        height: 13.0,
                        width: 16.0,
                        child: Image.asset(
                          CustomIcons.forwardarrow,
                          height: 10,
                          width: 13,
                          color: AppThemes.appMainThemeColor(),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Container(
                          child: Text(
                            "Number of matches attended",
                            style: AppThemes.textStyle(3.5).copyWith(
                                fontWeight: FontWeight.w700,
                                color: Colors.black),
                          ),
                        )),
                        Text(
                          "12",
                          style: AppThemes.textStyle(3.5)
                              .copyWith(color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Divider(
                    color: AppThemes.dividerColor(),
                    height: 1,
                    thickness: 1,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Container(
                          child: Text(
                            "Number of grounds visited",
                            style: AppThemes.textStyle(3.5).copyWith(
                                fontWeight: FontWeight.w700,
                                color: Colors.black),
                          ),
                        )),
                        Text(
                          "7/92",
                          style: AppThemes.textStyle(3.5)
                              .copyWith(color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Divider(
                    color: AppThemes.dividerColor(),
                    height: 1,
                    thickness: 1,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Container(
                          child: Text(
                            "Distance covered",
                            style: AppThemes.textStyle(3.5).copyWith(
                                fontWeight: FontWeight.w700,
                                color: Colors.black),
                          ),
                        )),
                        Text(
                          "201 mi",
                          style: AppThemes.textStyle(3.5)
                              .copyWith(color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Divider(
                    color: AppThemes.dividerColor(),
                    height: 1,
                    thickness: 1,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Container(
                          child: Text(
                            "Number of away matches",
                            style: AppThemes.textStyle(3.5).copyWith(
                                fontWeight: FontWeight.w700,
                                color: Colors.black),
                          ),
                        )),
                        Text(
                          "5",
                          style: AppThemes.textStyle(3.5)
                              .copyWith(color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Divider(
                    color: AppThemes.dividerColor(),
                    height: 1,
                    thickness: 1,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Container(
                          child: Text(
                            "Number of home matches",
                            style: AppThemes.textStyle(3.5).copyWith(
                                fontWeight: FontWeight.w700,
                                color: Colors.black),
                          ),
                        )),
                        Text(
                          "7",
                          style: AppThemes.textStyle(3.5)
                              .copyWith(color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Divider(
                    color: AppThemes.dividerColor(),
                    height: 1,
                    thickness: 1,
                  ),
                  SizedBox(
                    height: 60,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _EditButton() {
    return Container(
      width: 130,
      height: 35,
      child: filledButtonwithArrow(
          text: "Edit Profile",
          fontSize: SizeConfig.safeBlockHorizontal * 4,
          radius: 20.0,
          textColor: AppThemes.whiteColor,
          function: () {
            CommonClass.navigateToScreen(context, ProfileBottomPage());
          }),
    );
  }
}
