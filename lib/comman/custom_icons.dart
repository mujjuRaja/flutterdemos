class CustomIcons {
  //Login
  static const logInPath = "assets/images/login";
  static const splashImage = "$logInPath/image_splash.png";
  static const smallLogoIcon = "$logInPath/ic_small_logo.png";
  static const emailIcon = "$logInPath/ic_email.png";
  static const passwordIcon = "$logInPath/ic_password.png";
  static const facebookIcon = "$logInPath/ic_facebook.png";
  static const gmailIcon = "$logInPath/ic_gmail.png";
  static const twitterIcon = "$logInPath/ic_twitter.png";

  //Sign up
  static const signUpPath = "assets/images/signup";
  static const userNameIcon = "$signUpPath/ic_username.png";
  static const postCodeIcon = "$signUpPath/ic_postcode.png";
  static const postCodeNewIcon = "$signUpPath/ic_postcode_new.png";
  static const sportTrophyIcon = "$signUpPath/ic_sport_trophy.png";
  static const teamJerseyIcon = "$signUpPath/ic_team_jersey.png";
  static const gpsIcon = "$signUpPath/ic_gps.png";
  static const downArrowIcon = "$signUpPath/ic_down_arrow.png";

  //Home
  static const homePath = "assets/images/home";
  static const myTripIcon = "$homePath/ic_my_trip.png";
  static const forestIcon = "$homePath/forest.png";
  static const avfcIcon = "$homePath/avfc.png";
  static const searchIcon = "$homePath/ic_search.png";
  static const driveIcon = "$homePath/ic_drive.png";
  static const driveCarIcon = "$homePath/ic_drive_car.png";
  static const driveLocIcon = "$homePath/ic_drive_location.png";
  static const priceTagIcon = "$homePath/ic_price_tag.png";
  static const parkAndRideIcon = "$homePath/ic_park_and_ride.png";
  static const transitIcon = "$homePath/ic_transit.png";
  static const pubIcon = "$homePath/ic_pubs.png";
  static const foodAndDrinkIcon = "$homePath/ic_food_and_drink.png";
  static const parkingIcon = "$homePath/ic_parking.png";
  static const hotelIcon = "$homePath/ic_hotels.png";
  static const bookmakersIcon = "$homePath/ic_bookmakers.png";
  static const transitBusIcon = "$homePath/ic_transit_bus.png";
  static const transitTrainIcon = "$homePath/ic_transit_train.png";
  static const parkingDemoIcon = "$homePath/ic_parking_demo.png";
  static const pubPlaceholderIcon = "$homePath/ic_pub_placeholder.png";
  static const websiteIcon = "$homePath/ic_website.png";
  static const grayMarkerIcon = "$homePath/ic_pin_gray.png";
  static const orangeMarkerIcon = "$homePath/ic_pin_orange.png";
  static const grayMarkeriOSIcon = "$homePath/ic_pin_gray_ios.png";
  static const orangeMarkeriOSIcon = "$homePath/ic_pin_orange_ios.png";
  static const transitArrowIcon = "$homePath/ic_transit_forward_arrow.png";
  static const swipeArrowIcon = "$homePath/ic_swipe_arrow.png";
  static const transitGroupIcon = "$homePath/ic_transit_group.png";
  static const transitFootballIcon = "$homePath/ic_transit_football.png";
  static const placeHotelIcon = "$homePath/ic_place_hotel.png";
  static const placeFoodDrinkIcon = "$homePath/ic_place_foodDrink.png";
  static const placeParkingIcon = "$homePath/ic_place_parking.png";
  static const placePubIcon = "$homePath/ic_place_pubs.png";
  static const placeSearchIcon = "$homePath/ic_search_icon.png";
  static const wifiFacilityIcon = "$homePath/ic_wifi_facility.png";
  static const tvFacilityIcon = "$homePath/ic_tv_facility.png";
  static const clockIcon = "$homePath/ic_clock.png";
  static const userGroupIcon = "$homePath/ic_user_group.png";
  static const galleryImgIcon = "$homePath/ic_gallery_image.png";
  static const callIcon = "$homePath/ic_phone_call.png";
  static const offerIcon = "$homePath/ic_offer.png";
  static const stadiumImg = "$homePath/img_stadium.png";
  static const booking_dot_com = "$homePath/booking_dot_com.png";

  //Profile
  static const profilePath = "assets/images/profile";
  static const password = "$profilePath/password.png";
  static const terms = "$profilePath/terms.png";
  static const privacy = "$profilePath/privacy.png";
  static const faqs = "$profilePath/faqs.png";
  static const contacts = "$profilePath/contacts.png";
  static const notification = "$profilePath/notification.png";
  static const trash = "$profilePath/trash.png";
  static const logout = "$profilePath/logout.png";
  static const call = "$profilePath/call.png";
  static const email = "$profilePath/email.png";

  static const setting = "$profilePath/settings.png";
  static const forwardarrow = "$profilePath/forwardarrow.png";

  static const user = "$profilePath/user.png";
  static const cake = "$profilePath/cake.png";
  static const envelope = "$profilePath/envelope.png";
}
