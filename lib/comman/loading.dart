import 'package:flutter/material.dart';
import 'package:flutter_app/comman/app_themes.dart';


class Loading extends StatelessWidget {
  const Loading();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppThemes.mainAppColor),
        ),
      ),
      color: Colors.white.withOpacity(0.8),
    );
  }
}