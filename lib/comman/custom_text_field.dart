import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/comman/size_config.dart';

import 'app_themes.dart';

class CustomTextField extends StatelessWidget {
  CustomTextField(
      {this.onFieldSubmitted,
      this.icon,
      this.hint,
      this.labelText,
      this.obscure = false,
      this.validator,
      this.onSaved,
      this.onChanged,
      this.color,
      this.suffixColor,
      this.suffixIcon,
      this.keyBoardType,
      this.decoration,
      this.initialValue,
      this.maxLine,
      this.maxLength,
      this.controller,
      this.inputFormatters,
      this.prefixText,
      this.leftPadding,
      this.rightPadding,
      this.textInputAction,
      this.textCapitalization,
      this.prefixWidget,
      this.suffixWidget,
      this.enable,
      this.suffixWidgetClicked});

  Function suffixWidgetClicked;
  final FormFieldSetter<String> onFieldSubmitted;
  final FormFieldSetter<String> onSaved;
  final FormFieldSetter<String> onChanged;
  final Icon icon;
  final String hint;
  final String labelText;
  final bool obscure;
  final InputDecoration decoration;
  final FormFieldValidator<String> validator;
  final Color color;
  final Color suffixColor;
  final GestureDetector suffixIcon;
  final TextInputType keyBoardType;
  final String initialValue;
  final int maxLine;
  final int maxLength;
  final TextEditingController controller;
  final List<TextInputFormatter> inputFormatters;
  final String prefixText;
  final double leftPadding;
  final double rightPadding;
  final TextInputAction textInputAction;
  final TextCapitalization textCapitalization;
  final bool enable;
  final String prefixWidget;
  final String suffixWidget;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.only(
        left: leftPadding != null ? leftPadding : 5.0,
        right: rightPadding != null ? rightPadding : 5.0,
        top: 5.0,
        bottom: 5.0,
      ),
      child: TextFormField(
        enabled: enable != null ? enable : true,
        textCapitalization: textCapitalization == null
            ? TextCapitalization.none
            : textCapitalization,
        textInputAction: textInputAction,
        onFieldSubmitted: onFieldSubmitted,
        maxLength: maxLength,
        controller: controller,
        maxLines: maxLine,
        initialValue: initialValue,
        keyboardType: keyBoardType,
        onSaved: onSaved,
        onChanged: onChanged,
        validator: validator,
        obscureText: obscure,
        inputFormatters: inputFormatters,
        style: AppThemes.textStyleNormalWith(
          fontWeight: FontWeight.normal,
          fontSize: SizeConfig.safeBlockHorizontal * 3.5,
        ),
        decoration: InputDecoration(
            prefixText: prefixText,
            hintStyle: AppThemes.textStyleNormalWith(
              fontWeight: FontWeight.normal,
              fontSize: SizeConfig.safeBlockHorizontal * 3.5,
              color: AppThemes.textFieldPlaceholderColor,
            ),
            hintText: hint,
            labelText: labelText,
            enabledBorder: new OutlineInputBorder(
              borderSide: new BorderSide(
                color: color,
              ),
            ),
            border: new OutlineInputBorder(
              borderSide: new BorderSide(
                color: color,
              ),
            ),
            prefixIcon: prefixWidget != null
                ? Container(
                    padding: EdgeInsets.all(13.0),
                    child: Image.asset(
                      prefixWidget,
                      height: 15.0,
                      width: 15.0,
                    ),
                  )
                : null,
            suffixIcon: suffixWidget != null
                ? GestureDetector(
                    child: Container(
                      padding: EdgeInsets.all(13.0),
                      child: Image.asset(
                        suffixWidget,
                        height: 15.0,
                        width: 15.0,
                        color: suffixColor,
                      ),
                    ),
                    onTap: () {
                      suffixWidgetClicked();
                    },
                  )
                : null),
      ),
    );
  }
}
