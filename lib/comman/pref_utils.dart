import 'package:shared_preferences/shared_preferences.dart';

class Preferences {

  static final String userDetails = "is_my_flutter_app";
  static final String isLogin = "isLoginOrNot";
  static final String fcmId = "is_fcm_id";


  static Future<bool> setBool(String key, bool data) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(key, data);
  }

  static Future<bool> getBool(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key);
  }

  static Future<bool> setString(String key, String data) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, data);
  }

  static Future<String> getString(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  static Future<bool> clearData(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.remove(key);
  }
}
