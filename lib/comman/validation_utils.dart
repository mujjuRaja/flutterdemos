import 'package:flutter_app/comman/strings.dart';

class Validations {
  static String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (value.length == 0){
      return Strings.emptyemail;
    } else  if (!regex.hasMatch(value)) {
      return Strings.invalidemail;
    }
    return null;
  }

  static String validateMobile(String value) {
    String pattern = r'(^\+[1-9]{1}[0-9]{3,14}$)';

    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Please enter Mobile number';
    } else if (!regExp.hasMatch("+$value")) {
      return 'Please enter valid Mobile number';
    }
    return null;
  }

  static String validateRequired(String value) {
    if (value.trim().length == 0)
      return 'Required';
    else
      return null;
  }
}
