class Strings {
  static final emptyemail = "Please enter Email";
  static final invalidemail = "Please enter valid Email";
  static final emptypassword = "Please enter Password";
  static final emptyValidation = "Password length should be minimum 8";
  static final emptyCurrpassword = "Please enter current Password";
  static final emptyNewPassword = "Please enter new Password";
  static final emptyConfPassword = "Please enter confirm Password";
  static final passwordNotMatch = "New password not match with confirm Password";
  static final emptyDOB = "Please enter date of birth";
  static final emptyFullname = "Please enter your name";

  static final emptyfullname = "Please enter Full Name";
  static final emptypincode = "Please enter Postcode";
  static final emptysport = "Please select Sport";
  static final emptyteam = "Please Select Team";
  static final firstsportselect = "Please Select Sports first";
  static final passwordspace = "Password should not contain any space";
  static final validpostcode = "Please enter a valid postcode";

  static final emailTtl="Email";
  static final passwordTtl="Password";
  static final forgotPwTtl="Forgot Password?";

}