import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/comman/app_themes.dart';

import 'package:flutter_app/model/response_login.dart';
import 'package:flutter_app/viewmodel/loginViewModel.dart';
import 'package:flutter_app/comman/custom_button.dart';
import 'package:flutter_app/comman/size_config.dart';

import 'package:flutter_app/comman/color_extenstion.dart';

class CommonClass {
  static bool isUserLoggedIn;

  static Future<dynamic> navigateToScreen(
      BuildContext context, Widget screen) async {
    var value = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => screen));
    return value;
  }

  static navigateReplaceToScreen(BuildContext context, Widget screen) {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => screen));
  }

  static Future<dynamic> navigateAndRemoveNavigation(
      BuildContext context, String tag) async {
    var value = await Navigator.of(context)
        .pushNamedAndRemoveUntil(tag, (Route<dynamic> route) => false);
    return value;
  }

  static Widget progressIndicator() {
    return Container(
      height: 55,
      child: Center(
        child: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(
            AppThemes.mainAppColor,
          ),
        ),
      ),
    );
  }

  static void hideKeyBoard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  //Login User Auth Data
  static UserDetails authUser;

  static showSportBottomSheet(BuildContext context, List<SportsData> sportlist,
      Function callback) async {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        enableDrag: false,
        isDismissible: false,
        context: context,
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter modelState
                  /*You can rename this!*/) {
            return sportsDialogView(context, modelState, sportlist, callback);
          });
        });
  }

  static Widget sportsDialogView(BuildContext context, StateSetter modelState,
      List<SportsData> sportlist, Function callback) {
    return Container(
      color: Colors.transparent,
      margin: EdgeInsets.all(10.0),
      child: Card(
        color: AppThemes.whiteColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      callback("failure");
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      child: Icon(
                        Icons.arrow_back,
                        color: AppThemes.darkGrayColor,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Text(
                    "Select Sports",
                    style: AppThemes.textStyleNormalWith(
                      fontWeight: FontWeight.w600,
                      fontSize: SizeConfig.safeBlockHorizontal * 5,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              Expanded(
                child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child: Container(
                    child: ListView.builder(
                      physics: AlwaysScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: sportlist.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            modelState(() {
                              sportlist[index].isSelected =
                                  !sportlist[index].isSelected;
                            });
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 2),
                            padding: EdgeInsets.symmetric(
                                vertical: 15.0, horizontal: 20.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(5.0),
                              ),
                              border: Border.all(
                                color:
                                    selectedColour(sportlist[index].isSelected),
                              ),
                            ),
                            child: Text(
                              sportlist[index].sport,
                              style: AppThemes.textStyleNormalWith(
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: filledButton(
                  text: "SUBMIT",
                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                  radius: 5.0,
                  textColor: AppThemes.whiteColor,
                  function: () {
                    callback("success");
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  static AppBar profileAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              AppThemes.mainAppColor,
              AppThemes.redColor,
            ],
          ),
        ),
      ),
      backgroundColor: AppThemes.mainAppColor,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () {

          Navigator.pop(context);
        },
      ),
      title: Text(
        "Take Your Seat",
        style: AppThemes.getAppBarTitleTextStyle(),
      ),
    );
  }

  static Color selectedColour(bool isSelected) {
    if (isSelected)
      return AppThemes.mainAppColor;
    else
      return Colors.transparent;
  }
}
