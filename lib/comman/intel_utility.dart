import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:flutter_app/comman/app_themes.dart';
import 'package:fluttertoast/fluttertoast.dart';

class IntelUtility {
  static void showLoader(bool show, BuildContext context) {
    if (show) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return WillPopScope(
              onWillPop: () {},
              child: Center(
                child: SpinKitRing(
                  color: AppThemes.mainAppColor,
                  size: 55,
                ),
              ),
            );
          });
    } else {
      Navigator.of(context, rootNavigator: true).pop("");
    }
  }

  static Future<Coordinates> getCoordinatesFromPinCode(String pincode) async {
    try {
      if (pincode == null) return null;
      var addresses = await Geocoder.local.findAddressesFromQuery(pincode);
      if (addresses == null) {
        return null;
      }
      return addresses.first.coordinates;
    } catch (e) {
      return null;
    }
  }

  static bool isValidEmail(String email) {
    bool result = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
    return result;
  }

  static showNotificationAlert(String message) {
    showSimpleNotification(
        Text(
          message,
          style: TextStyle(fontWeight: FontWeight.w500),
        ),
        background: Colors.white,
        foreground: Colors.black,
        slideDismiss: true,
        duration: Duration(seconds: 3));
  }

  static getConvertedDOB(String date) {
    if (date != "") {
      var dateAndTime = DateFormat("yyyy-MM-dd").parse(date.substring(0, 10));
      DateFormat format = DateFormat("dd-MM-yyyy");
      String converted = format.format(dateAndTime);
      return converted;
    } else {
      return "";
    }
  }

  static getConvertedDate(String date) {
    if (date != "") {
      var dateAndTime = DateFormat("dd-MM-yyyy").parse(date.substring(0, 10));
      DateFormat format = DateFormat("yyyy-MM-dd");
      String converted = format.format(dateAndTime);
      return converted;
    } else {
      return "";
    }
  }

  static formattedDate(String date) {
    if (date != "") {
      var dateAndTime = DateFormat("yyyy-MM-dd").parse(date.substring(0, 10));
      DateFormat format = DateFormat("EEEE, dd MMM yyyy");
      String converted = format.format(dateAndTime);
      return converted;
    } else {
      return "";
    }
  }

  static formattedTime(String date) {
    if (date != "") {
      var dateAndTime =
          DateFormat("HH:mm:ss").parse(date.substring(11, date.length));
      DateFormat format = DateFormat("HH:mm aa");
      String converted = format.format(dateAndTime);
      return converted;
    } else {
      return "";
    }
  }

  static navigatePushToScreen(BuildContext context, Widget screen) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => screen));
  }

  static navigateReplaceToScreen(BuildContext context, Widget screen) {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => screen));
  }

  static showAlertDialog(BuildContext context, String message) {
    // set up the buttons
    Widget okButton = FlatButton(
      child: Text("Yes"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget cancelButton = FlatButton(
      child: Text("No"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text(message),
      actions: [
        okButton,
        cancelButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static showAlertToast(BuildContext context, String message) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        backgroundColor: Colors.black,
        content: Text(message),
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  static showSuccessToast(BuildContext context, String message) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        backgroundColor: Colors.green,
        content: Text(message),
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  static showFailedToast(BuildContext context, String message) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        backgroundColor: Colors.red,
        content: Text(message),
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

/*static Future<dynamic> navigateAndRemoveNavigation(BuildContext context, String tag) async {
    var value = await Navigator.of(context).pushNamedAndRemoveUntil(tag, (Route<dynamic> route) => false);
    return value;
  }*/

}

typedef APICallback(bool success, dynamic response);
