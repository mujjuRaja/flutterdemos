import 'package:flutter/material.dart';
import 'package:flutter_app/comman/color_extenstion.dart';
import 'package:flutter_app/comman/size_config.dart';

import 'size_config.dart';

class AppThemes {
  static final primaryFontFamily = "UniSans";

  static final Color mainAppColor = Color(0XFFEC9A34);
  static final Color redColor = Color(0XFFE15D2C);
  static final Color redBusinessColor = Color(0XFFF16424);
  static final Color whiteColor = Colors.white;
  static final Color darkGrayColor = Color(0XFF4E4E4E);
  static final Color lightGrayColor = Color(0XFFE6E6E6);
  static final Color blackColor = Color(0XFF151515);
  static final Color shadowColor = Color(0XACB1BF80);
  static final Color arrowColor = Color(0XFFC2C2C2);
  static final Color callAndEmailView = Color(0XFFF2F2F2);
  static final Color starYellowColor = Color(0XFFFDEC14);
  static final Color textFieldPlaceholderColor = Color(0XFFC6C6C6);

  static TextStyle textStyleNormalWith(
      {@required double fontSize,
        Color color,
        FontWeight fontWeight,
        TextDecoration decoration}) {
    return TextStyle(
        fontFamily: AppThemes.primaryFontFamily,
        fontSize: fontSize,
        color: color,
        fontWeight: fontWeight,
        decoration: decoration);
  }

  static TextStyle textStyleItalicWith(
      {@required double fontSize,
        @required Color color,
        FontWeight fontWeight,
        TextDecoration decoration}) {
    return TextStyle(
        fontFamily: AppThemes.primaryFontFamily,
        fontSize: fontSize,
        color: color,
        fontWeight: fontWeight,
        fontStyle: FontStyle.italic,
        decoration: decoration);
  }

  static TextStyle getAppBarTitleTextStyle() {
    return TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: SizeConfig.safeBlockHorizontal * 4.8,
      color: AppThemes.whiteColor,
      fontFamily: AppThemes.primaryFontFamily,
    );
  }

  static TextStyle titleItalic() {
    return TextStyle(
      fontSize: SizeConfig.safeBlockHorizontal * 4.5,
      fontWeight: FontWeight.w500,
      color: Colors.black,
      fontStyle: FontStyle.italic,
    );
  }

  static TextStyle textStyle(double size) {
    return AppThemes.textStyleNormalWith(
        fontSize: SizeConfig.safeBlockHorizontal * size);
  }

  static Color closeButtoncolor() {
    return HexColor("#707070");
  }

  static Color vsbgcolor() {
    return HexColor("#F6F6F6");
  }

  static Color dotColor() {
    return HexColor("#F07900");
  }

  static Color dotnormalColor() {
    return HexColor("#CFCFCF");
  }

  static Color appMainThemeColor() {
    return HexColor("#F16424");
  }

  static Color dividerColor() {
    return HexColor("#E6E6E6");
  }

  static Color listIconColor() {
    return HexColor("#D1D1D1");
  }
}
