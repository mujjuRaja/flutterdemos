import 'package:flutter/material.dart';
import 'package:flutter_app/comman/app_themes.dart';

import 'custom_icons.dart';

Widget filledButton(
    {String text,
    Color textColor,
    double radius,
    void function(),
    double fontSize}) {
  return InkWell(
    onTap: function,
    child: Container(
      height: 55,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            AppThemes.mainAppColor,
            AppThemes.redColor,
          ],
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(
            radius,
          ),
        ),
      ),
      child: Center(
        child: Text(
          text,
          style: AppThemes.textStyleNormalWith(
            fontWeight: FontWeight.w500,
            color: textColor,
            fontSize: fontSize,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    ),
  );
}

Widget filledButtonwithArrow(
    {String text,
    Color textColor,
    double radius,
    void function(),
    double fontSize}) {
  return InkWell(
    onTap: function,
    child: Container(
      height: 55,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            AppThemes.mainAppColor,
            AppThemes.redColor,
          ],
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(
            radius,
          ),
        ),
      ),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              text,
              style: AppThemes.textStyleNormalWith(
                fontWeight: FontWeight.w500,
                color: textColor,
                fontSize: fontSize,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              width: 5,
            ),
            Container(
              height: 13.0,
              width: 16.0,
              child: Image.asset(
                CustomIcons.forwardarrow,
                height: 10,
                width: 13,
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget callAndEmailButton({String image, double radius, void function()}) {
  return Container(
    width: 55,
    height: 55,
    decoration: BoxDecoration(
      gradient: LinearGradient(
        colors: <Color>[
          AppThemes.mainAppColor,
          AppThemes.redColor,
        ],
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(5),
      ),
    ),
    padding: EdgeInsets.symmetric(horizontal: 5.0),
    child: InkWell(
      onTap: () {},
      child: Image.asset(
        image,
        height: 10,
        width: 10,
        alignment: Alignment.center,
      ),
    ),
  );
}

Widget normalBorderButton(
    {String text,
    Color textColor,
    Color borderColor,
    double radius,
    void function(),
    double fontSize}) {
  return InkWell(
    onTap: function,
    child: Container(
      height: 55,
      decoration: BoxDecoration(
        border: Border.all(
          color: borderColor == null ? AppThemes.blackColor : borderColor,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(
            radius,
          ),
        ),
      ),
      width: double.infinity,
      child: Center(
        child: Text(
          text,
          style: AppThemes.textStyleNormalWith(
            fontWeight: FontWeight.w400,
            color: textColor,
            fontSize: fontSize,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    ),
  );
}
