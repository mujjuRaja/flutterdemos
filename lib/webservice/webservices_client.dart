import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_app/webservice/webservices.dart';

class WebserviceClient {
  static final version = "api_v1";

// this function is used to call the raw response from given url
// need to pass the full request url in this function
  static Future<dynamic> getAPIForRawResponse(String url) async {
    print("Get Url :" + url);
    var requrestUrl = Uri.parse(url);
    print("Get Url 2:" + requrestUrl.toString());

    var response = await http.get(requrestUrl).catchError((error) {
      return Future.error(error);
    });

    var jsValue = json.decode(response.body);
    print(jsValue);
    return jsValue;
  }

  static Future<ServerResponse> getAPICall(String apiName) async {
    var url = Webservices.baseUrl + version + apiName;
    print("Get Url :" + url);
    var postUri = Uri.parse(url);

    var header = Webservices.defaultHeaders();
    var completer = Completer<ServerResponse>();
    http
        .get(
      postUri,
      headers: header,

    )
        .then((response) {
      var jsValue = json.decode(response.body);
      ServerResponse serverResponse = ServerResponse.withJson(jsValue);
      completer.complete(serverResponse);
    }).catchError((error) {
      var response = ServerResponse();
      switch (error.runtimeType) {
        case SocketException:
          response.message =
              "Internet connection not available, check your connection and try again";
          break;
        default:
          response.message = error.toString();
          break;
      }
      response.isSuccess = false;
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<ServerResponse> postAPICall(
      String apiName, Map<String, dynamic> params) async {
    var url = Webservices.baseUrl + version + apiName;
    var postUri = Uri.parse(url);

    print("\n");
    print("Request URL: $url");
    print("Request parameters: $params");
    print("\n");

    var header = Webservices.defaultHeaders();

    var response = ServerResponse();
    var result = await http.post(postUri, body: params,headers: header).catchError((error) {
      response.isSuccess = false;
      response.message = handleError(error);
    });
    if (result != null) {

      print(result.body);
      var jsValue = json.decode(result.body);
      response = ServerResponse.withJson(jsValue);
    }

    return response;
  }

  static String handleError(dynamic error) {
    switch (error.runtimeType) {
      case SocketException:
        return "Internet connection not available, check your connection and try again";
        break;
      default:
        return error.toString();
        break;
    }
  }
}

class ServerResponse {
  var message = "Something went wrong!";
  var body;

  bool isSuccess;

  var statusCode = "";

  var offset = 0;

  ServerResponse();

  ServerResponse.withJson(Map<String, dynamic> jsonObj) {
    debugPrint("parsing response");
    var status = jsonObj["status"].toString();
    debugPrint("status ===== $status");
    this.message = jsonObj["message"];
    if (jsonObj["data"] != null)
      this.body = jsonObj["data"];
    else
      this.body = jsonObj;
    this.statusCode = status;
    this.isSuccess = status == "1";

    if (isSuccess) {
      if (jsonObj.containsKey("offset")) {
        offset = jsonObj["offset"];
      }
    }
    debugPrint("parsing response done");
  }
}
