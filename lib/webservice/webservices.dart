import 'dart:io';

import 'package:flutter_app/comman/commonClass.dart';
import 'package:flutter_app/comman/constant.dart';



class Webservices {
/*
  static final baseUrl = "http://172.104.44.157/takeyourseat/";

  //Authorization
  static final login = "/account/user_login";
  static final register = "/account/registration";
  static final getProfile = "/profile/get_profile";
  static final updateProfile = "/profile/edit_profile?";
*/

  //SAM app api
  static final baseUrl = "http://samapp.co/api/";

  //Authorization
  static final login = "LoginWithNumber";
  static final register = "register";
  static final getProfile = "userProfile";
  static final updateProfile = "updateUserProfile";


  static defaultHeaders() {
    return {
      "x-access-token": CommonClass.authUser?.token == null
          ? ""
          : CommonClass.authUser.token,
      HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
    };
  }
}
