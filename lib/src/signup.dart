import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/auth/homeScreen.dart';
import 'package:flutter_app/auth/loginScreen.dart';
import 'package:flutter_app/src/Widget/bezierContainer.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_app/comman/size_config.dart';
import 'package:flutter_app/comman/app_themes.dart';
import 'package:flutter_app/comman/custom_button.dart';
import 'package:flutter_app/comman/intel_utility.dart';
import 'package:flutter_app/comman/commonClass.dart';
import 'package:flutter_app/comman/strings.dart';
import 'package:flutter_app/comman/constant.dart';
import 'package:flutter_app/comman/validation_utils.dart';
import 'package:flutter_app/comman/pref_utils.dart';
import 'package:flutter_app/comman/custom_text_field.dart';
import 'package:flutter_app/comman/custom_icons.dart';

import 'package:flutter_app/viewmodel/loginViewModel.dart';

class SignUpPage extends StatefulWidget {
  SignUpPage(this.socialId, this._name, this._email, this.tabIndex);

  String socialId;
  String _name;
  String _email;
  int tabIndex;

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

final FirebaseAuth _auth = FirebaseAuth.instance;

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  // String teamId = "0", sportId = "0", selectedteams, selectedteamids;
  var concatenate = StringBuffer();

  // String selectedTeams = "";
  var mycolor = Colors.white;

  bool emptysport = true;

//  TextEditingController _sportController = TextEditingController();
  //TextEditingController _teamController = TextEditingController();
  TextEditingController _fullnameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  //TextEditingController postcodeController = TextEditingController();
  bool _success;
  LoginViewModel viewModel = LoginViewModel();

  Widget _entryField(String title, {bool isPassword = false}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
              //validator: (val) => val.isEmpty || !val.contains("@") ? "enter a valid eamil" : null,
              obscureText: isPassword,
              decoration: InputDecoration(
                  hintText: title,
                  border: InputBorder.none,
                  fillColor: Color(0xfff3f3f4),
                  filled: true))
        ],
      ),
    );
  }

  bool _isSocialSignupRequest = false;
  bool _isSocialEmailExist = false;

  _validateInput() {
    final FormState form = _formKey.currentState;
    if (_formKey.currentState.validate()) {
      //_register();
      IntelUtility.showLoader(true,context);
      signUp().then((pref) {
        IntelUtility.showLoader(false,context);
        if (pref != null) {
          if(pref =="Signed Up successfully."){
            IntelUtility.navigateReplaceToScreen(context, HomeScreen());
          }else{
            showSuccessFailedAlertDialog(context,pref);
          }

        } else {
          print("else auth data----===-> " + pref);
          showSuccessFailedAlertDialog(context,pref);
        }
      });

    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }

  showSuccessFailedAlertDialog(BuildContext context, String message) {
    // set up the buttons
    Widget continueButton = FlatButton(
      child: Text("Ok"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text(message),
      actions: [
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


  //When User "Signed Up", success snack will display.

  Future<String> signUp({String email, String password}) async {
    try {
      await _auth.createUserWithEmailAndPassword(
          email: _emailController.text, password: _passwordController.text);
      return "Signed Up successfully.";
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return "The password provided is too weak.";
      } else if (e.code == 'email-already-in-use') {
        return "The account already exists for that email.";
      } else {
        return "Something Went Wrong.";
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  doSignUpApi() {
    /* viewModel.fullname = fullnameController.text.toString().trim();
    viewModel.email = emailController.text.toString().trim();
    viewModel.password = passwordController.text.toString().trim();
    viewModel.postcode = postcodeController.text.toString().trim();
    viewModel.sport_id = sportId;
    viewModel.team_id = teamId;
    viewModel.selectedTeams = selectedTeams;
    IntelUtility.showLoader(true, context);

    viewModel.performRegisterApiCall((success, response) {
      IntelUtility.showLoader(false, context);

      if (success) {
        IntelUtility.showNotificationAlert(response);
        Navigator.pop(context);
      } else {
        IntelUtility.showNotificationAlert(response);
      }
    });*/
  }

  //...........................................
  Widget _inputFields() {
    return Container(
      child: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: Column(
          children: [
            CustomTextField(
              color: AppThemes.lightGrayColor,
              hint: "Enter Your Full Name",
              labelText: "Full Name",
              keyBoardType: TextInputType.text,
              validator: (value) =>
                  value.isEmpty ? Strings.emptyfullname : null,
              prefixWidget: CustomIcons.userNameIcon,
              controller: _fullnameController,
            ),
            CustomTextField(
              color: AppThemes.lightGrayColor,
              hint: "johnsmith@gmail.com",
              labelText: "Email",
              keyBoardType: TextInputType.emailAddress,
              validator: Validations.validateEmail,
              prefixWidget: CustomIcons.emailIcon,
              controller: _emailController,
              enable: _isSocialEmailExist ? false : true,
            ),
            _isSocialSignupRequest
                ? Container()
                : CustomTextField(
                    obscure: true,
                    maxLine: 1,
                    color: AppThemes.lightGrayColor,
                    hint: "Password",
                    labelText: "Password",
                    keyBoardType: TextInputType.text,
                    inputFormatters: [
                      //BlacklistingTextInputFormatter(RegExp('[ ]'))
                    ],
                    // ignore: missing_return
                    validator: (value) {
                      if (_isSocialSignupRequest == false) {
                        if (value.isEmpty) {
                          return Strings.emptypassword;
                        } else if (value.length < 8) {
                          return Strings.emptyValidation;
                        } else if (value.contains(" ")) {
                          return Strings.passwordspace;
                        } else {
                          return null;
                        }
                      }
                    },
                    prefixWidget: CustomIcons.passwordIcon,
                    controller: _passwordController,
                    enable: _isSocialSignupRequest ? false : true,
                  ),
            /*  CustomTextField(
              color: AppThemes.lightGrayColor,
              hint: "Enter Your Postcode",
              labelText: "Postcode",
              keyBoardType: TextInputType.text,

              prefixWidget: CustomIcons.postCodeIcon,
              suffixWidget: CustomIcons.gpsIcon,
              suffixColor: AppThemes.redBusinessColor,
              controller: postcodeController,
              suffixWidgetClicked: () {
               // _getCurrentLocation(true);
              },

              validator: (value) {
                bool isZipValid = RegExp(r"^[a-z0-9][a-z0-9\- ]{0,10}[a-z0-9]$",
                    caseSensitive: false)
                    .hasMatch(value);
                if (!_isSocialSignupRequest) {
                  if (value.isEmpty) {
                    return Strings.emptypincode;
                  } else if ((isZipValid == false)) {
                    return Strings.validpostcode;
                  } else if (value.length < 5) {
                    return Strings.validpostcode;
                  } else if (value.length > 8) {
                    return Strings.validpostcode;
                  } else {
                    return null;
                  }
                } else {
                  return null;
                }
              },
              // inputFormatters: <TextInputFormatter>[
              //   FilteringTextInputFormatter.digitsOnly
              // ],
            ),*/
            /* GestureDetector(
              onTap: () {
                CommonClass.hideKeyBoard(context);
                List<String> selectedSportsIds = List();
                for (var sport in viewModel.sportlist) {
                  selectedSportsIds.add(sport.sportId);
                }
                CommonClass.showSportBottomSheet(context, viewModel.sportlist,
                        (index) {
                      if (index == "failure") {
                        for (var teams in viewModel.sportlist) {
                          for (var sportIds in selectedSportsIds) {
                            if (teams.sportId == sportId) {
                              teams.isSelected = true;
                            } else {
                              teams.isSelected = false;
                            }
                          }
                        }
                      } else {
                        var selectedsports = "";
                        var selectedsportids = "";
                        for (var list in viewModel.sportlist) {
                          if (list.isSelected) {
                            if (selectedsports.isEmpty) {
                              selectedsports = list.sport;
                              selectedsportids = list.sportId;
                            } else {
                              selectedsports = selectedsports + ", " + list.sport;
                              selectedsportids =
                                  selectedsportids + "," + list.sportId;
                            }
                          }
                        }
                        sportId = selectedsportids;
                        _sportController.text = selectedsports;
                        _teamController.text = "";
                        //getCricketTeamList(selectedsportids);
                      }

                      setState(() {});
                    });
              },
              child: AbsorbPointer(
                child: CustomTextField(
                  controller: _sportController,
                  color: AppThemes.lightGrayColor,
                  hint: "Select Sports",
                  labelText: "Select Sports",
                  validator: (value) {
                    if (value.isEmpty) {
                      return Strings.emptysport;
                    } else {
                      emptysport = false;
                      return null;
                    }
                  },
                  prefixWidget: CustomIcons.sportTrophyIcon,
                  suffixWidget: CustomIcons.downArrowIcon,
                  suffixColor: AppThemes.redBusinessColor,
                ),
              ),
            ),*/
            /* GestureDetector(
              onTap: () {
                CommonClass.hideKeyBoard(context);
                if (_sportController.text.isEmpty) {
                  IntelUtility.showNotificationAlert(
                      "Please select sports first");
                } else {
                  bool isData = false;
                  */ /*for (var teams in viewModel.teamnamelist) {
                    if (teams.teamList.length > 1) {
                      isData = true;
                    }
                  }*/ /*
                 */ /* if (isData)
                    getTeamDropDown();
                  else
                    IntelUtility.showNotificationAlert(
                        "No team available for selected sports");*/ /*
                }
                // showTeamBottomSheet();
              },
              child: AbsorbPointer(
                child: CustomTextField(
                  controller: _teamController,
                  color: AppThemes.lightGrayColor,
                  hint: "Select Teams",
                  labelText: "Select Teams",
                  validator: (value) {
                    if (value.isEmpty) {
                      return Strings.emptyteam;
                    } else if (_sportController.text.isEmpty) {
                      return Strings.firstsportselect;
                    } else {
                      return null;
                    }
                  },
                  prefixWidget: CustomIcons.teamJerseyIcon,
                  suffixWidget: CustomIcons.downArrowIcon,
                  suffixColor: AppThemes.redBusinessColor,
                ),
              ),
            ),*/
          ],
        ),
      ),
    );
  }

  Widget _topView() {
    return Container(
      height: 130.0,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            AppThemes.mainAppColor,
            AppThemes.redColor,
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          IconButton(
            icon: Icon(
              Icons.arrow_back,
              size: 30.0,
              color: AppThemes.whiteColor,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          Spacer(),
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 30.0, right: 30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Register to",
                      style: AppThemes.textStyleItalicWith(
                        fontSize: SizeConfig.safeBlockHorizontal * 5,
                        color: Colors.black,
                      ),
                    ),
                    Text(
                      "Take Your Seat",
                      style: AppThemes.textStyleNormalWith(
                        fontSize: SizeConfig.safeBlockHorizontal * 6,
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              _isSocialSignupRequest
                  ? Container()
                  : InkWell(
                      onTap: () {
                        /*if (_isSocialSignupRequest) {
                    Preferences.setString(Preferences.userDetails,
                        responseLogInUserToJson(CommonClass.authUser));
                    CommonClass.navigateAndRemoveNavigation(
                        context, HomeScreen());
                  } else {
                    Constant.isUserSkipLogin = true;
                    CommonClass.navigateAndRemoveNavigation(
                        context, HomeScreen());
                  }*/
                      },
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 30.0, right: 30.0, bottom: 10.0, top: 20.0),
                        child: Row(
                          children: [
                            Text(
                              "Skip",
                              style: AppThemes.textStyleNormalWith(
                                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                                  fontWeight: FontWeight.w500,
                                  color: AppThemes.whiteColor,
                                  decoration: TextDecoration.underline),
                            ),
                            Icon(
                              Icons.arrow_forward_ios,
                              size: 10.0,
                              color: AppThemes.whiteColor,
                            )
                          ],
                        ),
                      ),
                    ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
        ],
      ),
    );
  }

  Widget _registerButton() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: filledButton(
        text: "REGISTER",
        fontSize: SizeConfig.safeBlockHorizontal * 4,
        radius: 5.0,
        textColor: AppThemes.whiteColor,
        function: _validateInput,
      ),
    );
  }

  Widget _alreadyAMemberButton() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: normalBorderButton(
        text: "ALREADY A MEMBER?",
        fontSize: SizeConfig.safeBlockHorizontal * 4,
        radius: 5.0,
        textColor: AppThemes.blackColor,
        function: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      onTap: () {
        CommonClass.hideKeyBoard(context);
      },
      child: Scaffold(
        backgroundColor: AppThemes.whiteColor,
        body: Column(
          children: [
            Container(
              height: 50.0,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    AppThemes.mainAppColor,
                    AppThemes.redColor,
                  ],
                ),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                physics: ClampingScrollPhysics(),
                child: Container(
                  child: Column(
                    children: [
                      _topView(),
                      Container(
                        margin: EdgeInsets.all(30.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            _inputFields(),
                            SizedBox(
                              height: SizeConfig.safeBlockVertical * 3,
                            ),
                            _registerButton(),
                            SizedBox(
                              height: SizeConfig.safeBlockVertical * 4,
                            ),
                            Container(
                              alignment: Alignment.center,
                              child: Text(_success == null
                                  ? ''
                                  : (_success
                                      ? 'Successfully registered ' +
                                          widget._email
                                      : 'Registration failed')),
                            ),
                            SizedBox(
                              height: SizeConfig.safeBlockVertical * 2,
                            ),
                            _alreadyAMemberButton(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
