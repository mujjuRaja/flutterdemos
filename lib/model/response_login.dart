// To parse this JSON data, do
//
//     final responseLogInUser = responseLogInUserFromJson(jsonString);

import 'dart:convert';

UserDetails responseLogInUserFromJson(String str) => UserDetails.fromJson(json.decode(str));

String responseLogInUserToJson(UserDetails data) => json.encode(data.toJson());

class UserDetails {
  UserDetails({
    this.userId,
    this.email,
    this.dob,
    this.fullName,
    this.pincode,
    this.sportId,
    this.teamId,
    this.token,
    this.userSelectedSportTeam,
  });

  int userId;
  String email;
  String dob;
  String fullName;
  String pincode;
  int sportId;
  int teamId;
  String token;
  List<UserSelectedSportTeam> userSelectedSportTeam;

  factory UserDetails.fromJson(Map<String, dynamic> json) => UserDetails(
    userId: json["user_id"] == null ? null : json["user_id"],
    email: json["email"] == null ? null : json["email"],
    dob: json["dob"] == null ? null : json["dob"],
    fullName: json["full_name"] == null ? null : json["full_name"],
    pincode: json["pincode"] == null ? null : json["pincode"],
    sportId: json["sport_id"] == null ? null : json["sport_id"],
    teamId: json["team_id"] == null ? null : json["team_id"],
    token: json["token"] == null ? null : json["token"],
    userSelectedSportTeam: json["user_selected_sport_team"] == null ? null : List<UserSelectedSportTeam>.from(json["user_selected_sport_team"].map((x) => UserSelectedSportTeam.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId == null ? null : userId,
    "email": email == null ? null : email,
    "dob": dob == null ? null : dob,
    "full_name": fullName == null ? null : fullName,
    "pincode": pincode == null ? null : pincode,
    "sport_id": sportId == null ? null : sportId,
    "team_id": teamId == null ? null : teamId,
    "token": token == null ? null : token,
    "user_selected_sport_team": userSelectedSportTeam == null ? null : List<dynamic>.from(userSelectedSportTeam.map((x) => x.toJson())),
  };
}

class UserSelectedSportTeam {
  UserSelectedSportTeam({
    this.id,
    this.sportId,
    this.userId,
    this.teamId,
    this.allTeam,
    this.createdOn,
  });

  String id;
  String sportId;
  String userId;
  String teamId;
  String allTeam;
  String createdOn;

  factory UserSelectedSportTeam.fromJson(Map<String, dynamic> json) => UserSelectedSportTeam(
    id: json["id"] == null ? null : json["id"],
    sportId: json["sport_id"] == null ? null : json["sport_id"],
    userId: json["user_id"] == null ? null : json["user_id"],
    teamId: json["team_id"] == null ? null : json["team_id"],
    allTeam: json["all_team"] == null ? null : json["all_team"],
    createdOn: json["created_on"] == null ? null : json["created_on"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "sport_id": sportId == null ? null : sportId,
    "user_id": userId == null ? null : userId,
    "team_id": teamId == null ? null : teamId,
    "all_team": allTeam == null ? null : allTeam,
    "created_on": createdOn == null ? null : createdOn,
  };
}
