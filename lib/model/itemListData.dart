import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ItemListData {
  static final listTitles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];

  static final listCategory = [
    'bike',
    'book',
    'car',
    'electronic',
    'fashion',
    'furniture'
  ];

  static final listCategoryIcons = [
    "assets/images/bike_blc.jpg",
    "assets/images/book_blc.jpg",
    "assets/images/car_blc.jpg",
    "assets/images/electronic_blc.jpg",
    "assets/images/fashion_blc.jpg",
    "assets/images/furniture_blc.jpg",
  ];

  static final listProductIcons = [
    "assets/static/ic_bike.png",
    "assets/static/ic_book.png",
    "assets/static/ic_car.png",
    "assets/static/ic_camera_app.png",
    "assets/static/ic_fashion_app.png",
    "assets/static/ic_furniture_app.png",
    "assets/static/pets_app.png",
  ];

  static final listIcons = [
    Icons.directions_bike,
    Icons.directions_boat,
    Icons.directions_bus,
    Icons.directions_car,
    Icons.directions_railway,
    Icons.directions_run,
    Icons.directions_subway,
    Icons.directions_transit,
    Icons.directions_walk
  ];
}
