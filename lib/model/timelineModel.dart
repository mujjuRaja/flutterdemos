
class TimelineModel {
  final String id;
  final String title;
  final String description;

  const TimelineModel({this.id, this.title, this.description});
}