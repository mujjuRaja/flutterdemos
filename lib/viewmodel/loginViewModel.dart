import '../comman/intel_utility.dart';
import '../comman/constant.dart';
import '../comman/strings.dart';
import '../comman/commonClass.dart';
import '../model/response_login.dart';
import '../webservice/webservices.dart';
import '../webservice/webservices_client.dart';
import '../comman/pref_utils.dart';

class LoginViewModel {

  var email = "";
  var password = "";
  var fullname = "";
  var postcode = "";
  var sport_id = "";
  var team_id = "";
  var selectedTeams = "";

  List<SportsData> sportlist = List();

  void performLogin(APICallback callback) {
    var params = {"email": email, "password": password.toString()};
    var validationResult = validateLogin();

    if (validationResult.isValid) {
      WebserviceClient.postAPICall(Webservices.login, params).then((response) {
        if (response.isSuccess) {
          print("Response body: " + response.body.toString());
          UserDetails responseLogin = UserDetails.fromJson(response.body);
          print("Response: " + responseLogin.toString());

          Preferences.setBool(Preferences.isLogin, true);

          CommonClass.authUser = responseLogin;
          Preferences.setString(Preferences.userDetails, responseLogInUserToJson(responseLogin));


          callback(true, response.message);
        } else {
          print("Response Message::::::---=-> " + response.message.toString());
          callback(false, response.message.toString());
        }
      }).catchError((error) {
        callback(false, error.toString());
      });
    } else {
      callback(false, validationResult.message);
    }
  }

  ValidationResult validateLogin() {
    if (email.isEmpty) {
      return ValidationResult(false, Strings.emptyemail);
    }
    if (!IntelUtility.isValidEmail(email)) {
      return ValidationResult(false, Strings.invalidemail);
    }
    if (password.isEmpty) {
      return ValidationResult(false, Strings.emptypassword);
    }
    return ValidationResult(true, "success");
  }

  performRegisterApiCall(APICallback callback) {
    var param = {
      "full_name": fullname,
      "email": email,
      "password": password,
      "pincode": postcode,
      "sport_id": sport_id,
      "team_id": team_id,
      "device_token": "",
      "sport_team_ids": selectedTeams
    };

    var validationresult = validationRegister();

    if (validationresult.isValid) {
      WebserviceClient.postAPICall(Webservices.register, param)
          .then((response) {
        print("Response Message RG:---=---> " + response.message.toString());

        if (response.isSuccess) {
          print("Response body: " + response.body.toString());
          callback(true, response.message);
        } else {
          callback(false, response.message.toString());
        }
      }).catchError((error) {
        callback(false, error.toString());
      });
    } else {
      callback(false, validationresult.message);
    }
  }

  ValidationResult validationRegister() {
    if (fullname.isEmpty) {
      return ValidationResult(false, Strings.emptyFullname);
    }
    if (email.isEmpty) {
      return ValidationResult(false, Strings.emptyemail);
    }
    if (!IntelUtility.isValidEmail(email)) {
      return ValidationResult(false, Strings.invalidemail);
    }
    if (password.isEmpty) {
      return ValidationResult(false, Strings.emptypassword);
    }
    if (postcode.isEmpty) {
      return ValidationResult(false, Strings.emptypincode);
    }
    if (sport_id.isEmpty) {
      return ValidationResult(false, Strings.emptysport);
    }
    if (team_id.isEmpty) {
      return ValidationResult(false, Strings.emptyteam);
    }
    return ValidationResult(true, "success");
  }
}

class SportsData {
  SportsData({
    this.id,
    this.sportId,
    this.sport,
    this.sportUniqueName,
    this.active,
    this.createdOn,
  });

  bool isSelected = false;
  String id;
  String sportId;
  String sport;
  String sportUniqueName;
  String active;
  String createdOn;

  factory SportsData.fromJson(Map<String, dynamic> json) => SportsData(
    id: json["id"] == null ? null : json["id"],
    sportId: json["sport_id"] == null ? null : json["sport_id"],
    sport: json["sport_name"] == null ? null : json["sport_name"],
    sportUniqueName: json["sport_unique_name"] == null
        ? null
        : json["sport_unique_name"],
    active: json["active"] == null ? null : json["active"],
    createdOn: json["created_on"] == null ? null : json["created_on"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "sport_id": sportId == null ? null : sportId,
    "sport": sport == null ? null : sport,
    "sport_unique_name": sportUniqueName == null ? null : sportUniqueName,
    "active": active == null ? null : active,
    "created_on": createdOn == null ? null : createdOn,
  };
}

