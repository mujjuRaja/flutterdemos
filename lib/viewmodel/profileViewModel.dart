import 'package:flutter/material.dart';
import 'package:flutter_app/comman/constant.dart';
import 'package:flutter_app/comman/intel_utility.dart';
import 'package:flutter_app/model/response_login.dart';

import '../comman/commonClass.dart';
import '../comman/pref_utils.dart';
import '../comman/strings.dart';
import '../webservice/webservices.dart';
import '../webservice/webservices_client.dart';

class ProfileViewModel {
  var email = "";
  var fullName = "";
  var dob = "";

  void getProfileDetails(APICallback callback, {Function onInactiveAccount}) {
    WebserviceClient.getAPICall(Webservices.getProfile).then((response) {
      if (response.isSuccess) {
        if (response.body != null) {
          UserDetails responseLogInUser = UserDetails.fromJson(response.body);
          CommonClass.authUser = responseLogInUser;
          print("response token : " + responseLogInUser.token);
          Preferences.setString(Preferences.userDetails,
              responseLogInUserToJson(responseLogInUser));
          callback(true, response.message);
        } else {
          callback(false, response.message);
        }
      } else if (int.parse(response.statusCode) == 403) {
        callback(false, response.message);
      } else {
        callback(false, response.message);
      }
    }).catchError((error) {
      print(error);
      callback(false, error.toString());
    });
  }

  void performUpdateProfile(APICallback callback) {
    var param = {"dob": dob, "email": email, "full_name": fullName};

    var validationResult = isValidateDetails();
    if (validationResult.isValid) {
      WebserviceClient.postAPICall(Webservices.updateProfile, param)
          .then((response) {
        if (response.isSuccess) {
          UserDetails responseLogInUser = UserDetails.fromJson(response.body);
          CommonClass.authUser = responseLogInUser;
          Preferences.setString(Preferences.userDetails, responseLogInUserToJson(responseLogInUser));
          callback(true, response.message);
        } else {
          callback(false, response.message);
        }
      }).catchError((error) {
        callback(false, error.toString());
      });
    } else {
      callback(false, "All fileds is required.");
    }
  }

  isValidateDetails() {
    if (dob.isEmpty) {
      return ValidationResult(false, Strings.emptyDOB);
    }
    if (email.isEmpty) {
      return ValidationResult(false, Strings.emptyemail);
    }

    if (!IntelUtility.isValidEmail(email)) {
      return ValidationResult(false, Strings.invalidemail);
    }
    if (fullName.isEmpty) {
      return ValidationResult(false, Strings.emptyfullname);
    }
    return ValidationResult(true, " Success");
  }
}
