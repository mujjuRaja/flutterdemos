import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NewTransaction extends StatefulWidget {
  final Function addTx;
  NewTransaction(this.addTx);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction>{

showAlertDialog(BuildContext context, String message) {
  // set up the buttons
  Widget cancelButton = FlatButton(
    child: Text("Ok"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );
  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Alert Message"),
    content: Text(message),
    actions: [
      cancelButton,
    ],
  );
  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

final titleController = TextEditingController();
final amountController = TextEditingController();


void submitData(BuildContext context) {
  final enterTitle = titleController.text;
  final enterAmount = amountController.text;

  if (enterTitle.isEmpty) {
    showAlertDialog(context, "Please enter Title");
    return;
  } else if (enterAmount.isEmpty) {
    showAlertDialog(context, "Please enter Amount.");
    return;
  }
  widget.addTx(
    enterTitle,
    double.parse(enterAmount),
  );

  Navigator.of(context).pop();
}

@override
Widget build(BuildContext context) {
  return Card(
    elevation: 5,
    child: Container(
      padding: EdgeInsets.all(5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          TextField(
            decoration: InputDecoration(labelText: 'Enter Title'),
            controller: titleController,
            onSubmitted: (_) => submitData(context),
          ),
          TextField(
            decoration: InputDecoration(labelText: 'Enter Amount'),
            controller: amountController,
            keyboardType: TextInputType.number,
            onSubmitted: (_) => submitData(context),
          ),
          FlatButton(
            child: Text('Add transaction'),
            textColor: Colors.purple,
            onPressed: () {
              submitData(context);
            },
          )
        ],
      ),
    ),
  );
}

}
